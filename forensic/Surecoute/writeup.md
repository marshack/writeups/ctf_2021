## Sur-écoute

## Consignes :  

Le serveur de messagerie d'une organisation suspecte a été mis sur écoute.
Vulnérable à la faille Heartbleed, la clef privée du serveur a pu être récupérée.

Aidez les services de renseignements à découvrir ce que prépare l'organisation suspecte, grâce à cette capture réseau.


## Points attribués :  
```
10 Points
```

## Flag :  
```
Fl@g{Venkon}
```

## Architecture :  
```
Néant
```

## Déroulé :  

```
1. Ouvrir le fichier capture.pcap avec wireshark.
2. Dans wireshark, menu Editer > Préférences, puis Protocols > TLS > Edit... > +
3. 83.193.140.180 | 25 | smtp | key.pem > OK
4. Dans le listing des paquets, choisir un paquet TLSv1.2 > clic droit > Suivre > TLS Stream
5. Cliquer sur "Save as" > smtp.txt, puis fermer wireshark
6. Editer le fichier smtp.txt, lire le message, puis supprimer les 15 permières lignes, et les 4 dernières lignes, et sauver le fichier en message.eml
7. Depuis le fichier message.eml extraire les pièces jointes (2 images JPEG encodées en base64), soit à la main, soit avec l'outil munpack (apt-get install mpack)
8. Ouvrir le fichier "jcdekmjkmjolcgjb.jpg", puis examiner les données exif avec exiftool (exiftool jcdekmjkmjolcgjb.jpg)
9. Repérer le champ Certificate encodé en base64, le décoder => "stegosuite 0.7"
10. Télécharger stegosuite http://stegosuite.org
11. Exécuter stegosuite (java -jar stegosuite-0.7-<syst>_amd64.jar) et ouvrir l'image "jcdekmjkmjolcgjb.jpg"
12. Trouver que le mot de passe est celui de la question posée dans le quiz, à savoir "RAFALE", et cliquer sur Extract
13. Récupérer les 2 fichiers instrukcioj.txt (à traduire de l'espéranto) et celo.jpg (on détient la première partie de la communication)
14. Voir la réponse de john.doe@gmail.com et regarder la deuxième pièce jointe (signature.jpg). La signature possède un champ (le nom) dans le même format que la réponse à la question du quiz => mot de passe.
15. Exécuter stegosuite (java -jar stegosuite-0.7-<syst>_amd64.jar) et ouvrir l'image "signature.jpg"
16. Saisir le mot de passe "John Doe", et récupérer le flag.
```





