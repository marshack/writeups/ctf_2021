﻿# Garde aux 3 drapeaux - Niveau 2


## Consignes :


[Ce challenge nécessite d'avoir terminé le challenge "Garde aux 3 drapeaux - Niv.1".
L'image obtenue en fin de niveau 1 servira de base pour ce challenge.]

Félicitations, vous avez pu nous aider à ouvrir correctement l'image PNG !
Cependant, comme vous avez pu vous en apercevoir, il semblerait qu'un problème lors de la transmission nous empêche de visualiser le bas de l'image.

Pourriez-vous nous aider à reconstituer le bas de l'image ?

Ah oui, la station distante qui nous a transmis le fichier, nous a également transmis ses logs.
Je ne sais pas s'ils pourront vous être utiles, mais dans le doute :

```
2020-01-21 20:52:39 INFO - Start of transmission
2020-01-21 20:54:43 ERROR - Failed to transmit bytes :  20:44:59:96:a9:ca:2a:42:a8:79:ac:4e:b8:bd:78:03:41:00:50:da:8e:2b:2e:8a:4e:46:8d:f8:3c:86:a2:fa:fb:50:41:3b:8e:74:83:d3:6e:f9:95:e5:78:6f:20:28:05:c2:82:e8:61:79:01:4e:af:ca:4a:d9:43:c9:1f:88
2020-01-21 20:55:30 DEBUG - 1878881 bytes transmitted
2020-01-21 20:55:30 INFO - End of transmission
```

## Points attribués :

```
20 Points
```

## Flag :

```
fl@g{reserve_operationnelle_de_cyberdefense} 
```

## Déroulé :

On part de l'image obtenue en fin de niveau 1 :

![](https://172.24.40.13/r.nicol/challenge/raw/branch/master/forensic/Garde%20aux%203%20drapeaux%20-%20Niv.2/assets/Garde%20aux%203%20drapeaux_repaired.png)


Une première analyse avec l'outil pngcheck (distribué sous debian/ubuntu) nous permet de faire le point sur cette image :

```
> $ pngcheck -v "Garde aux 3 drapeaux_repaired.png"
> 
> File: Garde aux 3 drapeaux_repaired.png (1878881 bytes)
>   chunk IHDR at offset 0x0000c, length 13
>     3307 x 2339 image, 24-bit RGB, non-interlaced
>   chunk IDAT at offset 0x00025, length 65536
>     zlib: deflated, 32K window, default compression
>   chunk IDAT at offset 0x10031, length 65536
>   chunk IDAT at offset 0x2003d, length 65536
>   chunk IDAT at offset 0x30049, length 65536
>   chunk IDAT at offset 0x40055, length 65536
>   chunk IDAT at offset 0x50061, length 65536
>   chunk IDAT at offset 0x6006d, length 65536
>   chunk IDAT at offset 0x70079, length 65536
>   chunk IDAT at offset 0x80085, length 65536
>   chunk IDAT at offset 0x90091, length 65536
>   chunk IDAT at offset 0xa009d, length 65536
>   chunk IDAT at offset 0xb00a9, length 65536
>   chunk IDAT at offset 0xc00b5, length 65536
>   chunk IDAT at offset 0xd00c1, length 65536
>   chunk IDAT at offset 0xe00cd, length 65536
>   chunk IDAT at offset 0xf00d9, length 65536
>   chunk IDAT at offset 0x1000e5, length 65536
>   chunk IDAT at offset 0x1100f1, length 65536
>   chunk IDAT at offset 0x1200fd, length 65536
>   chunk IDAT at offset 0x130109, length 65536
>   chunk IDAT at offset 0x140115, length 65536
>   CRC error in chunk IDAT (computed 1c8011f7, expected 0d23454d)
> ERRORS DETECTED in Garde aux 3 drapeaux_repaired.png
```

On s'aperçoit que le chunck situé à l'offset 0x140115 est corrompu.
En analysant les logs donnés en consigne, on pose l'hypothèse que des octets manquent dans ce chunck.

Pour vérifier l'hypothèse, on regarde la taille de ce chunk dans le fichier : on trouve 65 472 octets.
Or, ce chunck doit normalement faire 65 536 octets.
Il manque donc bien 64 octets et c'est exactement la taille de l'extrait que l'on trouve dans les logs (donnés en consigne).

Chaque chunk possède un checksum en CRC32 qui permet de vérfier son contenu. On code alors un script qui insère l'extrait de 64 octets dans le chunk corrompu à toute les positions possibles. A chaque position essayée, on calcule le checksum et on le compare avec le crc32 donné par le fichier.

Voici un script python permettant de réaliser ceci :
```python
import zlib

addr = 0x140115 # start of corrupted IDAT

piece = b"\x20\x44\x59\x96\xa9\xca\x2a\x42\xa8\x79\xac\x4e\xb8\xbd\x78\x03\x41\x00\x50\xda\x8e\x2b\x2e\x8a\x4e\x46\x8d\xf8\x3c\x86\xa2\xfa\xfb\x50\x41\x3b\x8e\x74\x83\xd3\x6e\xf9\x95\xe5\x78\x6f\x20\x28\x05\xc2\x82\xe8\x61\x79\x01\x4e\xaf\xca\x4a\xd9\x43\xc9\x1f\x88"

with open('Garde aux 3 drapeaux_repaired.png', 'rb') as fin:
  fin.read(addr - 4) # jump to corrupted IDAT
  length = int.from_bytes(fin.read(4), byteorder='big', signed=False)
  type = fin.read(4) # read IDAT
  data = fin.read(length - len(piece))
  crc = int.from_bytes(fin.read(4), byteorder='big', signed=False)

  assert(type == b"IDAT")
  fin.read(4) # length of next chunk
  assert(fin.read(4) == b"IDAT")

found = False
for pos in range(len(data)): # pos is the index where inserting the piece
  print("\rTry position {} of {}".format(pos+1, len(data)), end='')
  candidate = data[:pos] + piece + data[pos:]
  assert(len(candidate) == length)
  if zlib.crc32(type + candidate) == crc:
    found = True
    break

print()

if not found:
  exit()

with open('Garde aux 3 drapeaux_repaired.png', 'rb') as fin:
  with open('Garde aux 3 drapeaux_repaired_and_filled.png', 'wb') as fout:
    fout.write(fin.read(addr + 4 + pos)) # copy-paste until position identified
    fout.write(piece)
    fout.write(fin.read())
```

Une fois le chunk corrigé, le flag est lisible directement sur l'image.


