# Garde aux 3 drapeaux - Niveau 3


## Consignes :


[Ce challenge nécessite d'avoir terminé le challenge "Garde aux 3 drapeaux - Niv.2".
L'image obtenue en fin de niveau 2 servira de base pour ce challenge.]

Félicitations, vous avez réussi à réparer l'image dans sa totalité.
Malheureseument, sans que nous sachions quoi, il nous manque une information capitale dans ce message.

Nous pressentons qu'il y ait un message stéganographié dans l'image obtenue.
Pourriez-vous vérifier ce point pour nous ?


## Points attribués :
```
30 Points
```

## Flag :

```
fl@g{crpoc.cer.fct@intradef.gouv.fr}
```

## Déroulé :

Pour chaque outil, indiquer les commandes d'installation pour permettre une configuration correcte du conteneur.)_

Un indice se trouve littéralement sur l'image dans l'encadré de gauche.
On peut y lire :

```
La stéganographie est l'art de la dissimulation : son objet est de faire passer inaperçu un message dans un autre message. La technique de base - dite LSB pour least significant bit - consiste à modifier le bit de poids faible des pixels codant une image.
```

Sans cet indice, il faut penser au LSB comme technique de stéganographie. Ce qui reste une méthode classique sur une image à compression sans perte.

Le script Python suivant nous permet d'étudier les bits de poids faibles :

```python
from PIL import Image, ImageDraw

# Extract first bit layer
img = Image.open('Garde aux 3 drapeaux_repaired_and_filled.png')
img = img.convert("RGB")
draw = ImageDraw.Draw(img)

for y in range(img.height):
  print("\rLines processed : {} / {}".format(y+1, img.height), end='')
  for x in range(img.width):
    px = img.getpixel((x, y))
    px = tuple((i & 0b1) * 255 for i in px)
    draw.point((x,y), px)

print()

img.save('first bit layer.png', 'PNG')
```

Ce script extrait le bit de poids faible de chaque pixel de l'image. Ce qui donne le résultat suivant :

![](https://172.24.40.13/r.nicol/challenge/raw/branch/master/stegano/Garde%20aux%203%20drapeaux%20-%20Niv.3/assets/first%20bit%20layer.png)

On voit très clairement que ce résultat n'est pas normal. Tous les pixels sont nuls (dans les trois couleurs) sauf quelques lignes en bas de l'image qui alternent des pixels blancs et noirs.
Il doit donc y avoir de la donnée de codée à l'identique sur les trois canaux (RGB).

On vérifie également le deuxième bit de poids faible de chaque pixel afin d'être sûr qu'une partie du message ne soit pas également codé dessus.
On le fait en modifiant la ligne :
```python
px = tuple((i & 0b1) * 255 for i in px)
```
par
```python
px = tuple(((i & 0b10) >> 1) * 255 for i in px)
```

Ce qui donne le résultat suivant :

![](https://172.24.40.13/r.nicol/challenge/raw/branch/master/stegano/Garde%20aux%203%20drapeaux%20-%20Niv.3/assets/second%20bit%20layer.png)

Ce plan paraît normal et ne semble pas contenir de message caché.

On extrait donc la suite de 0 et de 1 à partir des bits de poids faible à l'aide de ce script :

```python
from PIL import Image, ImageDraw
import base64

img = Image.open('Garde aux 3 drapeaux_repaired_and_filled.png')
img = img.convert("RGB")

binary = ""
for y in range(img.height):
  print("\rLines read : {} / {}".format(y+1, img.height), end='')
  line = ""
  for x in range(img.width):
    px = img.getpixel((x, y))
    line += str(px[0] & 0b1)

  if line.count('1'):
    binary += line

print()
```

Puis on convertit chaque groupe de 8 bits en caractère ASCII :

```python
b64 = ""
for i in range(0, len(binary), 8):
  if binary[i:i+8].count('1'):
    b64 += chr(int(binary[i:i+8], 2))
```

En affichant ce résultat, on s'aperçoit rapidement que l'on a affaire à un message encodé en base64.
On l'affiche, décodé en utf-8 avec la dernière ligne suivante :

```python
print(base64.b64decode(b64).decode("utf-8"))
```

On obtient enfin le message suivant :


*Cette année, 40 réservistes opérationnels participent pendant une semaine à l’exercice majeur de cyberdéfense des armées, DEFNET, sur la base de Rochefort. Sur une plateforme dédiée, ils s’exercent aux procédures militaires de gestion de crise. Tout au long de l’année, pour maintenir un haut degré d’excellence, le ministère fait appel à des renforts ponctuels apportant une expertise et des compétences ciblées, provenant du secteur civil. Les réservistes sont employés dans des postes opérationnels au sein des entités spécialisées en cyber : le centre des opérations cyber, le centre d’analyse en lutte informatique défensive (CALID), le centre des audits de la sécurité des systèmes d’information (CASSI), le centre support de cyberdéfense de la Marine, ...*

*"L’augmentation des risques de cyberattaques pouvant toucher la défense depuis quelques années m’a amené à penser je  pouvais faire quelque chose à mon niveau! Je voulais découvrir le monde militaire tout en mettant mes compétences au service de mon pays." Patrice est réserviste opérationnel depuis mars 2017. Il travaille pour un grand groupe français : "Ces périodes de réserve m’ont apporté tout d’abord sur le plan humain mais également sur le plan technique car pour résoudre un problème on raisonne  en équipe et chacun apporte ses connaissances pour résoudre le problème. Pour moi l’intérêt est mutuel, c’est un cercle vertueux dont l’entreprise bénéficie également car j’en reviens enrichi en expérience."*

*Nathan est réserviste opérationnel depuis novembre 2017. Architecte réseaux et systèmes dans le civil, il renforce chaque année le COMCYBER : "L’armée m’apprend beaucoup humainement, car dans mon emploi civil je travaille seul ou en très petit groupe alors que dans la cyberdéfense militaire on travaille en collectif, on s’entraide à résoudre les problèmes, on partage et on enrichit nos connaissances techniques, cela enrichi mon expérience."*

*François est réserviste opérationnel depuis juin 2016, en doctorat en Data sciences, basé en Ile-de-France : "Issu d’une famille de militaires, je n’avais pas forcément envie de m’engager mais j’avais envie de mieux connaître le monde militaire. J’ai justement appris que l’armée pouvait être intéressée par mon domaine de compétences en matière de  cyberdéfense. Ce compromis entre mes études civiles et un peu de temps consacré à la cyberdéfense me convient parfaitement, je serai même prêt à  m’impliquer davantage dans des projets très concrets si mon profil de spécialiste en Data sciences et intelligence artificielle est jugé utile. Ce qui me plaît vraiment sur DEFNET c’est de découvrir l’homogénéité des différents profils qui s’adaptent et se complètent pour résoudre un problème."*

*Le commandement de la cyberdéfense (COMCYBER) s’est doté d’une réserve d’emploi, la réserve de cyberdéfense, qui vient renforcer en compétence l’ensemble des unités du COMCYBER. La réserve de cyberdéfense regroupe à la fois des réservistes opérationnels et des réservistes citoyens. Le réserviste opérationnel souscrit un engagement à servir dans la réserve opérationnelle, un contrat rémunéré d'une durée de 1 à 5 ans renouvelable. Ces volontaires font le choix de servir leur pays sans faire du métier des armes leur seule profession.*

*Les conditions pour être réserviste sont : d’être de nationalité française et résider en France, d’avoir fait sa JAPD/JDC, de ne pas avoir de casier judiciaire et d’avoir des compétences dans les NTIC. Pour entrer dans la réserve, il faut postuler (CV + lettre de motivation) à l’adresse suivante : fl@g{crpoc.cer.fct@intradef.gouv.fr}*

