import os
from graphviz import Digraph

os.environ["PATH"] += os.pathsep + r".\Graphviz2.38\bin"

ROOT_PATH = os.getcwd()

os.chdir("..")

ARBO_PATH_DIRECTORY = os.path.join(os.getcwd(), 'source', 'arbo')

os.chdir(ARBO_PATH_DIRECTORY)

# Trie des dossiers selon leur ordre de création
dirs_names = os.listdir()
dirs_names.sort(key=lambda x:x.split(' - ')[1])

# Création d'un graphique avec Graphviz
g = Digraph('G', filename='tree.gv', directory=ROOT_PATH, format='png', node_attr={'shape': 'plaintext'}, edge_attr={'color': 'white'})

# Création d'un noeud représentant le répertoire ROOT
g.node('0', '#')

# Parcours du nom de tous les répertoires du premier niveau
for i, dir_name in enumerate(dirs_names, start=1):

    if int(dir_name.split(' - ')[0]):

        g.node(str(i), '#')

    else:

        g.node(str(i), '')

    # Parcours récursif de chaque répertoire du premier niveau
    for j, (root, dirs_names, files) in enumerate(os.walk(dir_name, topdown=True), start=1):

        # S'il existe des sous-répertoire du répertoire du premier niveau
        if dirs_names:

            # Si le répertoire est à représenter dans l'arboresence / True / 1
            if int(dirs_names[0].split(' - ')[0]):

                # TODO : A perfectionner concernant le choix de l'id
                # Création d'un noeud avec un id (id n'est pas un int mais un str) composé de 5 fois l'id du répertoire de premier niveau
                # Plus l'id de chaque n niveau du répertoire
                g.node(str(i) * 5 + str(j), '#')

            # False / 0
            else:

                g.node(str(i) * 5 + str(j), '')

    # print([(str(i), str(i) + '1'),
    #          (str(i) + '1', str(i) + '2'),
    #          (str(i) + '2', str(i) + '3'),
    #          (str(i) + '3', str(i) + '4'),])

    # Lien entre le répertoire ROOT et les répertoires de premier niveau
    g.edge('0', str(i))
    # Liens entre le répertoire de premier niveau et les sous-répertoires de n niveaux
    g.edges([(str(i), str(i) * 5 + '1'),
             (str(i) * 5 + '1', str(i) * 5 + '2'),
             (str(i) * 5 + '2', str(i) * 5 + '3'),
             (str(i) * 5 + '3', str(i) * 5 + '4'),])

# Affichage du graphique
g.view()

