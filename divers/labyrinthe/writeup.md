
# Labyrinthe

## Consignes :  


Trouvez le plus court chemin. 



## Points attribués :  
```
60 Points
```

## Flag :  
```
Fl@g{Br3adth F1rst S3arch}
```

## Déroulé :  

Vous vous connectez à l'adresse <ip><port>

```
nc <ip> <port>

 __  __                        _    _            _    
|  \/  |                 ____ | |  | |          | |   
| \  / | __ _ _ __ ___  / __ \| |__| | __ _  ___| | __
| |\/| |/ _` | '__/ __|/ / _` |  __  |/ _` |/ __| |/ /
| |  | | (_| | |  \__ \ | (_| | |  | | (_| | (__|   < 
|_|  |_|\__,_|_|  |___/\ \__,_|_|  |_|\__,_|\___|_|\_\
                        \____/                        


Sample :

┌───────┬───────────────────┐
S v     │                   │
│ v ╶───┘   ┌───┬───────────┤
│ v         │   │           │
│ v ╶───────┤   ╵   ╷   ╶───┤
│ > > > > v │       │       │
├───────┐ v └───┐   └───┐   │
│       │ > > v │       │   │
│   ╶───┴───╴ v └───────┘   │
│             > > > > > > > E
└───────────────────────────┘

Answer :

   Shortest way : Svvvv>>>>vv>>vv>>>>>>>E


┌───────────────┬───────────────────────────────┬───────────────────────┬───────┐
S               │                               │                       │       │
│   ╷   ╶───┐   └───┐   ╶───────┐   ╶───┬───╴   └───────╴   ┌───────┐   │   ╷   │
│   │       │       │           │       │                   │       │   │   │   │
├───┴───╴   ├───╴   ├───╴   ┌───┴───╴   ├───────────┬───┬───┘   ╶───┤   │   │   │
│           │       │       │           │           │   │           │   │   │   │
│   ┌───────┤   ╶───┼───────┘   ┌───╴   │   ┌───┐   │   ╵   ┌───┐   ╵   │   │   │
│   │       │       │           │       │   │   │   │       │   │       │   │   │
│   │   ┌───┴───╴   │   ╶───┬───┘   ┌───┘   │   ├───┘   ┌───┘   └───────┘   │   │
│   │   │           │       │       │       │   │       │                   │   │
│   │   ╵   ╶───────┴───┐   │   ┌───┘   ┌───┘   │   ╶───┴───────┐   ┌───────┴───┤
│   │                   │   │   │       │       │               │   │           │
│   └───────┬───────┐   ╵   ├───┘   ┌───┘   ╶───┴───────────╴   │   ╵   ╶───┐   │
│           │       │       │       │                           │           │   │
│   ╶───┐   └───┐   ╵   ┌───┘   ┌───┤   ┌───────────────┬───────┤   ┌───────┘   │
│       │       │       │       │   │   │               │       │   │           │
├───╴   ├───┐   └───────┤   ┌───┘   │   ╵   ┌───────┐   ╵   ╷   └───┤   ╶───────┤
│       │   │           │   │       │       │       │       │       │           │
│   ╶───┘   └───────┐   ╵   │   ╶───┴───────┘   ╷   └───────┴───╴   └───────╴   │
│                   │       │                   │                               E
└───────────────────┴───────┴───────────────────┴───────────────────────────────┘


Enter path (30 s) ?

```

Les joueurs ont 30 secondes pour envoyer le plus court chemin pour résoudre le labyrinthe.
Le laps de temps étant court, le challenge ne peut se résoudre que par programmation.

2 scripts python 3 sont fournis :

```
challenge_maze.py : création du challenge  
exploit.py        : solution du challenge
```
challenge_maze.py :

```
Génére un labyrinthe différent à chaque connexion
   Utilisation de l'algorithme BFS pour générer le labyrinthe 
Utilisation du signal "time" pour arrêter le processus au bout de 30 secondes.

```

exploit.py :

```
Permet de résoudre le challenge.

Le script python 3 fonctionne sur ubuntu18 et kali.

Prérequis :
  installation de python3
  installation du module pwn 

  apt-get install python3-pip
  python3 -m pip  install git+https://github.com/arthaud/python3-pwntools.git   
    
```

Test et validation du challenge:


 - Test local:
```
utilisation de socat :

  socat TCP-LISTEN:30003,reuseaddr,fork EXEC:./challenge_maze.py

Pour se connecter au challenge :

  nc 127.0.0.1 30003
  
Modifier l'ip et le port dans le fichier exploit.py pour tester le challenge. 
```


 - Test sur plateforme

```
En attente d'intégration pour connaître l'ip et le port.
```



Résultat après exécution du script exploit.py

Ceci est un exemple de résultat : 

```
./exploit.py 
[+] Opening connection to 127.0.0.1 on port 30003: Done
┌───┬───────────────────────────────────┬───────────┬───────────────────────┬───┐
S   │                                   │           │                       │   │
│   │   ╷   ╶───┬───────────┬───╴   ┌───┘   ┌───╴   │   ╷   ╶───┬───────╴   │   │
│   │   │       │           │       │       │       │   │       │           │   │
│   ╵   ├───╴   ├───────┐   │   ┌───┘   ┌───┤   ╶───┘   └───┐   │   ╶───────┤   │
│       │       │       │   │   │       │   │               │   │           │   │
│   ╶───┤   ╶───┘   ╷   │   ╵   │   ┌───┘   ├───────────┬───┘   ├───────╴   │   │
│       │           │   │       │   │       │           │       │           │   │
├───┐   ├───────────┤   └───┐   │   │   ╷   └───────┐   │   ╶───┴───────┐   ╵   │
│   │   │           │       │   │   │   │           │   │               │       │
│   │   └───────╴   └───┐   ├───┘   │   └───┬───╴   ╵   ├───────────┐   ├───╴   │
│   │                   │   │       │       │           │           │   │       │
│   └───────┬───────┐   │   ╵   ┌───┘   ╷   └───────┐   ├───╴   ┌───┘   │   ┌───┤
│           │       │   │       │       │           │   │       │       │   │   │
│   ┌───╴   │   ╷   ╵   ├───────┘   ╶───┴───┬───╴   │   ╵   ╷   │   ┌───┤   │   │
│   │       │   │       │                   │       │       │   │   │   │   │   │
│   ├───────┤   ├───────┴───┐   ┌───────┬───┘   ╷   ├───┬───┴───┘   │   ╵   │   │
│   │       │   │           │   │       │       │   │   │           │       │   │
│   ╵   ╷   ╵   └───╴   ╷   └───┘   ╷   ╵   ┌───┘   │   ╵   ┌───────┘   ╶───┘   │
│       │               │           │       │       │       │                   E
└───────┴───────────────┴───────────┴───────┴───────┴───────┴───────────────────┘

Svvvv>>^^^^>>vv>>vv<<vv>>>>^^>>vvvv>>vvvv>>^^>>^^^^^^>>^^>>^^>>>>vv<<vv>>>>^^^^>>>>>>>>>>vv<<<<vv>>>>vvvv>>vv<<vvvvvv<<vv>>>>>E


b' path (30 s) ?\n'
b'Congratulations.\n'
b'   The flag is : Fl@g{Br3adth F1rst S3arch}\n'
[*] Closed connection to 127.0.0.1 port 30003

```

exploit.py

```python
#!/usr/bin/env python3
# -*- coding: utf-8 -*-


# F5 paste/no paste (copier coller sans indentation)
# F6 pour executer le script python 2.7
# F7 pour executer le script python 3
# F8 highlighting on/off, and show current value.


def DFS(x,y,Map):
    if (Map[x][y]=="exit"): #check if we're at the exit
        return [(x,y)] #if so then we solved it so return this spot
    if (Map[x][y]!="path"): #if it's not a path, we can't try this spot
        return []
    Map[x][y]="explored" #make this spot explored so we don't try again
    for i in [[x-1,y],[x+1,y],[x,y-1],[x,y+1]]: #new spots to try
            result = DFS(i[0],i[1],Map) #recursively call itself
            if len(result)>0: #if the result had at least one element, it found a correct path, otherwise it failed
                result.append((x,y)) #if it found a correct path then return the path plus this spot
                return result
    return [] #return the empty list since we couldn't find any paths from here

from collections import deque
def BFS(x,y,Map):
    #ipdb.set_trace(context=5)
    queue = deque( [(x,y,None,"?")]) #create queue
    while len(queue)>0: #make sure there are nodes to check left
        node = queue.popleft() #grab the first node
        x = node[0] #get x and y
        y = node[1]
        if Map[x][y] == "exit": #check if it's an exit
            return GetPathFromNodes(node) #if it is then return the path
        if (Map[x][y]!="path"): #if it's not a path, we can't try this spot
            continue
        Map[x][y]="explored" #make this spot explored so we don't try again
        for i in [[x-1,y,"^"],[x+1,y,"v"],[x,y-1,"<"],[x,y+1,">"]]: #new spots to try
            queue.append((i[0],i[1],node,i[2]))#create the new spot, with node as the parent
    return []
           
def GetPathFromNodes(node):
    path = []
    chemin = []
    while(node != None):
        #ipdb.set_trace(context=5)
        path.append((node[0],node[1],node[3]))
        node = node[2]

    chemin = [a[2] for a  in path]
    path = [(a[0],a[1]) for a in path]
    
    chemin.reverse()
   

    # affiche la solution 
    #print ("Solution")
    global solution
    solution = "S" + "".join([x for x in chemin[1:]])+"E"
    #print (solution)
    return path
       
def GetMap():
    
    global str_maze
    data = str_maze
  
    res=[]
    for count,ligne in enumerate(data): 
        res.extend([list(ligne.strip())])
    
    for x in range(len(res)):
        for y in range(len(res[x])):
           if (res[x][y]==ord("#")): res[x][y]="wall"   
           if (res[x][y]==ord(" ")): res[x][y]="path"   
           if (res[x][y]==ord("S")): res[x][y]="start"   
           if (res[x][y]==ord("E")): res[x][y]="exit"   
   
    #print (res)
    return res
    

from pwn import *
import time
import io


# Global
solution=""

#context.log_level="debug"

## site distant
hostname="127.0.0.1"
port=30003
p = remote(hostname,port)

time.sleep(1)

res = p.recvuntil("\x0a\x0a")
res = p.recvuntil("\x0a\x0a\x0a")
res = p.recvuntil("Enter")
res = res.decode('utf8')
res = (res[:-8])

print (res)

maze=""
for i in range(len(res)):
   if res[i] != " " and res[i] != "\n" and res[i] != "S" and res[i] !="E":
      maze=maze+"#"
   if res[i] =="S":
      maze=maze+"S"
   if res[i] =="E":
      maze=maze+"E"
   if res[i] == " ":
      maze=maze+" "
   if res[i] == "\n":
      maze=maze+"\n"


maliste=[]
maliste=maze.split("\x0a")

maze_ascii=""
for ligne in maliste:
  for i in range(0,len(ligne),2):
    maze_ascii = maze_ascii+ligne[i]
  maze_ascii = maze_ascii+"\n"

str_maze = maze_ascii
g=io.BytesIO()
g.write(str_maze.encode())
g.seek(0)
str_maze=g.readlines()



BFS(1,1,GetMap())
print ()
print (solution)

print()
print()

p.sendline(solution)
print (p.recvline(timeout=1))
print (p.recvline(timeout=1))
print (p.recvline(timeout=1))


p.close
```






