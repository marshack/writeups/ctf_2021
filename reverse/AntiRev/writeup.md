# AntiRev ##

## Consignes :  


Un fichier binaire à reverser.


## Points attribués :  

```
15 Points
```

## Flag :  

```
Fl@g{Gg&W2lLD@ne}
```

## Déroulé :  

Ici l'épreuve peut se résoudre 100% en analyse statique (pas besoin de lancer le binaire ou de le debugger, bien que ça soit aussi une solution possible au challenge).
Durée de résolution: entre 5-10mn pour l'habitué en reverse et 30-60mn pour un débutant.

- Antidebug Linux (ptrace bloqué) = pas strace possible, bypassable en controlant le flow d'exécution (un IF à changer au bon moment) - pas nécessaire pour résoudre.
- Antibruteforce (contournable si on patch le binaire -> un moyen de résoudre le chall peut-être?)
- Obfuscation des chaînes de caractères (pas nécessaire pour trouver la solution, fausse piste pour le challenger)
- Antidisassembly (graph view bloqué dans IDA, instructions non décodés correctement)
   
Actuellement la solution se trouve simplement en analyse statique. Après avoir bypass l'anti-disassembly (par exemple en forcant IDA à lire la data non alignée comme du code valide) on voit ici la validation de la chaîne de caractères en input, qui se fait char par char (aucune chaîne visible en utilisant `strings`):


    $ ./chall.bin 
    [+] Welcome to the RTFM challenge
    [+] Please enter the flag: aaaaaaaaaaaaaaaaaaaa
    SORRY FAILED
    $ ./chall.bin 
    [+] Welcome to the RTFM challenge
    [+] Please enter the flag: Fl@g{Gg&W2lLD@ne}
    WELL DONE


Une solution est d'utiliser Radare2 et de chercher les mnemoniques cmp.
```
r2 chall

[0x00000810]> aaa
[x] Analyze all flags starting with sym. and entry0 (aa)
[x] Analyze len bytes of instructions for references (aar)
[x] Analyze function calls (aac)
[x] Use -AA or aaaa to perform additional experimental analysis.
[x] Constructing a function name for fcn.* and sym.func.* functions (aan)

[0x00000810]> /c cmp (ou /ad/ cmp)
0x00000036   # 2: cmp byte [rax], al
0x00000080   # 2: cmp byte [rdx], al
0x0000084f   # 3: cmp rax, rdi
0x00000850   # 2: cmp eax, edi
0x000008d0   # 7: cmp byte [rip + 0x201751], 0
0x000008d1   # 5: cmp eax, 0x201751
0x000008d9   # 8: cmp qword [rip + 0x201717], 0
0x000008da   # 7: cmp dword [rip + 0x201717], 0
0x000008db   # 5: cmp eax, 0x201717
0x00000981   # 3: cmp eax, dword [rbp - 0xc]
0x000009cd   # 4: cmp dword [rbp - 0x18], 0
0x000009f3   # 4: cmp dword [rbp - 0x1c], 0
0x00000a0b   # 2: cmp al, 0x46
0x00000a1e   # 2: cmp al, 0x6c
0x00000a31   # 2: cmp al, 0x40
0x00000a44   # 2: cmp al, 0x67
0x00000a57   # 2: cmp al, 0x7b
0x00000a7d   # 2: cmp al, 0x67
0x00000a90   # 2: cmp al, 0x26
0x00000aae   # 2: cmp al, 0x57
0x00000ac1   # 2: cmp al, 0x32
0x00000ad4   # 2: cmp al, 0x6c
0x00000ae7   # 2: cmp al, 0x4c
0x00000b02   # 2: cmp al, 0x44
0x00000b11   # 2: cmp al, 0x40
0x00000b20   # 2: cmp al, 0x6e
0x00000b2f   # 2: cmp al, 0x65
0x00000b3e   # 2: cmp al, 0x7d
```

Les valeurs sont donc : 

```
46 6c 40 67 7b 67 26 57 32 6c 4c 44 40 6e 65 7d
Fl@g{g&W2lLD@ne}
```
