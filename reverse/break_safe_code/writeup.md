# Break safe's code

## Consignes :  


Vous vous êtes introduit dans un datacenter et essayez de récupérer les mots de passe présents dans deux coffres.
Vous avez réussi à ouvrir le premier au chalumeau, mais le deuxième vous résiste. Impossible de percer sa porte en naquadah !!!!

Seule solution possible : retrouver le code à partir du firmware prélevé sur le premier coffre, en espérant que ce soit le même (firmware/code).
Pour vous aider vous trouverez ci-joint un schéma électronique simplifié et une page du manuel utilisateur.


## Pièces jointes :  
- **Schéma électronique simplifié :**

![](assets/schema_electronique.png)


- **Diagramme de fonctionnement de la porte :**

    ![](assets/flowchart.png)

- **Mémoire flash du premier coffre fort :** coffrefort.bin

    Extraction du binaire de l'ELF via:

    ````bash
  avr-objcopy -O binary coffrefort.elf coffrefort.bin
  ````

## Points attribués :
```
60 points
```

## Déroulé :  

- **Spécifications des AVRs : **
  
  - Architecture Harvard: mémoire programme séparée de la RAM
  - Processeur RISC 8bits
  - Bus d'adressage 16bits
  - RAM séparée en deux parties: de 0x0000 à 0X0FF, registres de travail, d'états ou d'entrées/sorties. Au dessus (0x0100), mémoire RAM utilisable pour les variables et la pile.
  
- **Reconnaissance des ports d'entrés/sorties utilisés :** la première étape consiste à déterminer la fonctionnalité de chaque broche du µcontrôleur utilisée par le circuit électronique à l'aide du <u>**datasheet**</u>:
  
  ![atmega328pin](assets/atmega328pin.png)
  
  On en déduit les éléments suivants:
  
  - Le port D, des PINs 0 à 6, est exclusivement utilisé pour la lecture du clavier.
  
  - Le clavier forme une matrice où chaque bouton est relié directement à deux broches du contrôleur: une pour la colonne et l'autre pour la ligne.
  
  - Le buzzer et la diode électroluminescente sont connectés au port B PIN 5.
  
  - Le servomoteur, patte 16, est branché sur le port B PIN 2. Cette patte est aussi partagée avec OC1B piloté par le TIMER1 servant à générer des signaux en PWM. Il semblerait donc que ce composant soit contrôlé de façon matériel et que sa rotation se fasse via une reconfiguration du TIMER1.
  
    > **Pour information :** les servomoteurs se pilotent par une impulsion dont la largeur est proportionnelle à l'angle du bras.
    >
    > ![](assets/diss_servo.png)
  
- **Désassemblage :** Ouvrir le binaire avec **"cutter"**. Le firmware étant un binaire pur, il faut indiquer au décompilateur certains paramètres:
  
  - Niveau d'analyse: aaaa
  - Architecture:  AVR
  - CPU:  ATmega328p
  - Bits: 8
  
- **Vecteurs d'interruptions :** hormis la première ligne correspondant au reset, toutes les autres renvoient vers la ligne 0x0049 qui elle-même redirige les interruptions vers le début du programme. **On peut en conclure qu'il n'y a pas de routine d'interruption**.

- **EntryPoint :** le point d'entrée du programme permet d'initialiser les registres et les variables globales.

  ![](assets/diss_entrypoint.png)  

  - R1 à 0

  - Z est initialisé à 0x0278 (via R30 et R31)

  - X est initialisé à 0x0100 (via r26 et r27)

  - Via la boucle de droite, on effectue une copie d'un octet de la mémoire flash vers la mémoire RAM avec auto-incrémentation jusqu'à ce que X soit égal à 0x010E 

  - On recopie donc 14 octets de la mémoire flash à partir de l'adresse de base 0x0278 vers l'adresse RAM 0x0100.

    ![](assets/diss_varglobales.png)

    

    Ce qui donne en RAM:

    | Adresse | Contenu |
    | ------- | ------- |
    | 0x0100  | 0x10    |
    | 0x0101  | 0x01    |
    | 0x0102  | 0x10    |
    | 0x0103  | 0x08    |
    | 0x0104  | 0x40    |
    | 0x0105  | 0x08    |
    | 0x0106  | 0x40    |
    | 0x0107  | 0x02    |
    | 0x0108  | 0x04    |
    | 0x0109  | 0x20    |
    | 0x010A  | 0x04    |
    | 0x010B  | 0x01    |
    | 0x010C  | 0x40    |
    | 0x010D  | 0x20    |

  - Une fois les variables globales chargées, la fonction main est lancée (bloc de gauche)

- **Fonction main() :** le graphique de la fonction main ressemble beaucoup au diagramme de fonctionnement du coffre,  en comparant, il est possible de nommer les fonctions.

  - Première fonction: "verrouillage_porte()"
  - Deuxième fonction: "lecture_code()"
  - La partie droite semble retourner à la vérification du code, cela veut dire que cette fonction doit renvoyer un retour différent de 0 (R1 = 0, initialisé dans l'entrypoint).
  - Si la vérification est bonne on lance la fonction "ouverture_coffre"
  - Enfin, une boucle sur une fonction permettant de voir si la touche "#" est appuyé. Elle a a besoin de paramètres (R22 et R24), elle doit renvoyer (dans R24) une valeur différente de 0 pour sortir. On a donc la fonction "test_touche(param1, param2)". 

- **Paramètres de la fonction test_touche() :** les paramètres { 0x04, 0x08 } semblent correspondre à la touche "#". Comme vu précédemment, on peut voir que le clavier forme une matrice où chaque touche à ses propres coordonnées { ligne, colonne }. Reste à vérifier si { 0x04, 0x08 } sont bien des coordonnées.

  Pour le cas particulier de "#", on a :


  ![](assets/atmega328pin-diese.png)

  Si l'on retranscrit ce couple de chiffres en masque binairen, on a:

  - 0x04 -> 0000 00100 : cela correspond bien à la patte 2 du port D et donc à la broche 4 du µcontrôleur
  - 0x08 -> 0000 01000 : cela correspond bien à la patte 3 du port D et donc à la broche 5 du µcontrôleur

  On a donc: **char test_touche(maque_colonne, masque_ligne)**.

  > **NOTE :** <u>C'est là la grande difficulté du challenge</u>. Il faut que le joueur intuite qu'il a affaire à un double masque binaire sélectionnant une touche en particulier.

- **Fonction lecture_code() :**  cette fonction se découpe en deux blocs : une boucle à droite et une suite de tests à gauche terminant la fonction. Le passage d'un bloc à l'autre est dicté par la fonction **test_touche**(0x04, 0x08), vu précédemment, qui par l'appui sur la touche "#" finira la fonction.

  A droite, on peut apercevoir un autre appel à test_touche(), mais cette fois-ci avec des paramètres situés en mémoire RAM.

  - ldd r22, z+1
  - ld r24, z
  - call test_touche

  ![](assets/diss_test_touche_z.png)

  Il faut maintenant déterminer l'adresse de Z à la première itération, Z dépendant de R29:

  - En début de fonction R29 = 0
  - Initialisation de Z: Z = 0X0000 + R29 = 0x0000
  - Décalage a gauche de R30 et R31, équivalent à Z = Z*2 <=> Z = 0x0000
  - Soustraction à Z de 0xFF00, équivalent à Z = Z + 0x100 <=> Z = 0x0100

  L'adresse **0x0100** correspond à l'adresse de base des variables globales. On a donc lors de la première boucle **test_touche(*0x0100, *0x0101)**.

  Le compteur **R29** n'est modifié qu'en bas du bloc de droite:

  ![](assets/diss_R29plusplus.png)

  *subi r29, 0xFF* est l'équivalent d'un r29++. En reprenant la logique ci-dessus, on a donc à chaque boucle :

  - Z = 0x0000 + R29
  - Z = Z * 2
  - Z = 0x0100 + Z

  A chaque boucle, on avance de deux octets. Avec des paramètres de 1 octet, on a donc un tableau à deux dimensions dont l'adresse de base est 0x0100.

- **Détermination de la taille du tableau (et donc du code PIN) :**  Sur la partie de gauche, lors de l'appuis sur la touche "#", R29 est testé avec la valeur "0x07". Si elle n'est pas égale, R24 = 0x00 et la fonction lecture_code() échoue. Nous pouvons en conclure que le tableau a donc 7 lignes. Cela correspond à la taille des variables globales de 14 octets (7 * 2 octets).

  ![](assets/diss_tab_size.png)

- **Mapping de la RAM :** maintenant que nous savons que nous avons à faire à un tableau de 7 lignes et de deux colonnes, nous pouvons réécrire le tableau du début et en déduire le code du coffre.

  | Adresse | Colonne (Z) | Ligne (Z+1) | Touche |
  | ------- | ----------- | ----------- | ------ |
  | 0x0100  | 0x10        | 0x01        | **4**  |
  | 0x0102  | 0x10        | 0x08        | *****  |
  | 0x0104  | 0x40        | 0x08        | **0**  |
  | 0x0106  | 0x40        | 0x02        | **8**  |
  | 0x0108  | 0x04        | 0x20        | **3**  |
  | 0x010A  | 0X04        | 0x01        | **6**  |
  | 0x010B  | 0x40        | 0x20        | **2**  |


Le flag est donc : Fl@g{4*08362}