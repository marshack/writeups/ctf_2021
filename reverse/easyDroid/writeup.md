#  EasyDroid

## Consignes :  


Une application vient de vous être envoyée pour test.
Prouvez que le système présente des faiblesses.

## Points attribués :  

```
15 Points
```

## Flag :  

```
Fl@g{Andr01d_1nsid3}
```

### Déroulé :  


Le challenge a été développé via AndroidStudio et testé sur les version 4.2 et 7 d'Android.
Compilation avec paramètre par défaut et signé avec un certificat créé pour l'occasion.


Analyse du binaire :  

Le programme est testé sur un émulateur. 
Le fonctionnement est simple, il faut trouver une combinaison afin de faire apparaître le mot "correct".

<img src="assets/menu.png" />

Il n'y a aucun indice ce qui va nous diriger vers l'analyse du code source.


Extraction du code source avec jadx.

```
./jadx ~/Desktop/challenge.apk 
INFO  - loading ...
INFO  - processing ...
INFO  - done
```


Nous récupérons le fichier principal. 

```
more ./sources/com/example/myapplication/MainActivity.java
```

Le fichier n'est pas obfuscé ce qui rend plus facile la lecture.



La méthode vérif() prend en entrée les 4 valeurs issues des pressions des touches.
La première faille est le système de vérification ou une seule valeur suffit à remplir la condition :
```
TextView myAwesomeTextView = (TextView) MainActivity.this.findViewById(R.id.editText5);
                if (MainActivity.this.t.get(0).byteValue() == 102) {
                    myAwesomeTextView.setText("Correct");
                } else {
                    myAwesomeTextView.setText("Erreur");
                }

```
Cela n'est cependant pas suffisant.

La méthode verif() apporte des éléments de réponse :
```
 public List<Byte> verif(List<Byte> a2) {
        this.b = new ArrayList();
        byte[] flag = getResources().getString(R.string.tips).getBytes();
        for (Integer x = 0; x.intValue() < flag.length; x = Integer.valueOf(x.intValue() + 1)) {
            this.b.add(Byte.valueOf((byte) (flag[x.intValue()] ^ a2.get(x.intValue() % a2.size()).byteValue())));
        }
        return this.b;
    }

```

Il s'agit d'un xor effectué entre les valeurs issues des touches et une chaîne statique (R.string.tips).
Pour cela allons voir à quoi cette chaîne correspond.

```
more ./resources/res/values/strings.xml | grep tips
    <string name="tips">ttJ`iYdc`(;cM)dt{|9z</string>

```

Il s'agit donc de trouver les valeurs des touches pour avoir le flag.  
Nous avons plusieurs possibilités, le brute force sachant qu'il y a que 4 caractères ou bien un peu de logique.
Partant du principe que le flag commence par fl@g nous allons essayer de trouver la clé directement.  

```
chaine = "fl@g"
chaine_c = "ttJ`"

for i in range(len(chaine)):
	print(ord(chaine[i])^ord(chaine_c[i]))
 
18
24
10
7
```

Testons avec cette clé :

```
key = [18,24,10,7]
for i in range(len(chaine)):
...     print(chr(ord(chaine[i]) ^ key[i % len(key)]))
 
fl@g{Andr01d_1nsid3}
```

Voilà.
