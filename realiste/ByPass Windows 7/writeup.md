# ByPass Windows 7

## Consignes :

Nos commandos ont ramené un poste informatique de leur dernière opération. Des données hautement sensibles se trouvent sous le profil utilisateur du général auquel il appartenait. Malheureusement, le post-it sur lequel était indiqué le mot de passe est resté collé sous le clavier.  

Votre mission, si vous l'acceptez, vous connecter sur le poste et récupérer le flag. 


## Indice :

Le flag est bien dans le profil utilisateur, il n'a pas ete caché au fond d'un dossier systeme obscur ou autre.


## Points attribués

```
10/100
```

## Flag :

```
fl@g{MindomsADS}
```

## Déroulé :



Le premier but à atteindre est de se connecter à la VM.

Pour cela, on va envoyer un arrêt brutal lors du démarrage de la VM. 
Pendant le démarrage, on va cliquer sur le bouton redémarrer de la fenetre portailvms.

La VM redémarré et nous donne le choix entre:

 - Démarrer windows normalement.
 ou
 - Lancer l'outil de redémarrage système (recommandé).

On va choisir de lancer l'outil de redémarrage système.

Ouverture d'une fenetre de "réparation du démarrage", puis un pop-up demande si l'on préfère restaurer le systeme ou annuler la restauration.
On va choisir d'annuler.

" ... Petite musique d'ascenceur pour patienter ..."

Un dernier pop-up nous informe que l'outil ne peut pas réparer et qu'on peut envoyer ou pas des informations sur le problème.
Sur ce pop-up, on va déplier la flèche "voir des details sur ce problème"
Tout en bas du texte on va cliquer sur le lien vers la déclaration de confidentialité X:\windows\system32\fr-FR\erofflps.txt

Ouverture d'un bloc-notes.
On va faire fichier puis ouvrir.

Ouverture d'un explorateur de fichiers de la machine.
On va se rendre au chemin suivant: D:\Windows\System32
On va sélectionner tous les types de fichiers.

On renomme le fichier sethc en sethcOLD.
On renomme le fichier cmd en sethc.

On ferme les fenetres avec la croix (sinon la machine s'arrete et, sur cette plateforme, elle est détruite) et au pop-up demandant si on veut redémarrer la machine, on accepte.

Sur l'écran de login, on appuie 5 fois rapidemment sur la touche shift.
On ferme la fenêtre des touches rémanentes
On obtient une invite de commandes en tant que system.

On se créer un compte user avec la commande:
```
net user admin password /ADD
```

On met ce compte dans le groupe Administrateurs:
```
net localgroup Administrateurs admin /ADD
```

On redémarre la machine et on se connecte avec admin/password.

On va dans le profil du général Bole, dans le dossier mes documents.(c:\utilisateurs\Général Bole\Mes Documents)
On essaie d'ouvrir le fichier secret.txt, échec.

Dans les propriétés du fichier, on peut se donner tous les droits sur le fichier.
Clic droit propriété, onglet sécurité, continuer. "Ajouter" pour s'ajouter a la liste. On rajoute l'utilisateur admin et on lui accorde le contrôle total sur le fichier.
On ouvre le fichier secret.txt mais c'est un leurre.

On ouvre un cmd en tant qu'administrateur (attention, c'est toujours sethc.exe pour lancer cmd.exe).

On se déplace dans les documents du général Bole.
```
cd ../.. 
cd "users\Général Bole\Documents"
```

Avec un dir on ne voit que le fichier secret.txt.
Avec un dir /R on voit le fichier de flux de données alternatif.
On peut visualiser ce fichier avec la commande more : 
```
more < secret.txt:validation.txt.
```

On obtient le fl@g.



