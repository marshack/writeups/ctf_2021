# Tellement _Net

## Consignes:


Télécharger le fichier PCAP et récupérer le flag.


## Points attribués

```
10 Points
```

## Flag:

```
fl@g{darkshell}
```

## Déroulé:


Il faut ouvrir le fichier avec WireShark.

IL faut identifier le flux TCP "Telnet".

```
login:admin

Mdp:666c40677b6461726b7368656c6c7d
```

Il faut décoder le mot de passe qui est en "héxadécimal" en "texte":
```

66 :f

6c :l

40 :@

67 :g

7b :{

64 :d

61 :a

72 :r

6b :k

73 :s

68 :h

65 :e

6c :l

6c :l

7d :}

```






Vous êtes sur le bon chemin!

```56 6f 75 73 20 c3 aa 74 65 73 20 73 75 72 20 6c 65 20 62 6f 6e 20 63 68 65 6d 69 6e 21 ```

Vous brûlez!!

```20 56 6f 75 73 20 62 72 75 6c 65 7a 21 21```

Vous n'avez jamais été aussi proche de trouver la solution!!!

```20 56 6f 75 73 20 61 76 65 7a 20 6a 61 6d 61 69 73 20 c3 a9 74 c3 a9 20 73 69 20 70 72 6f 63 68 65 20 64 65 20 74 72 6f 75 76 65 72 20 6c 61 20 73 6f 6c 75 74 69 6f 6e 21 21 21 ```

Vous êtes presque arrivé !!!!

```56 6f 75 73 20 c3 aa 74 65 73 20 70 72 65 73 71 75 65 20 61 72 72 69 76 c3 a9 73 21 21 21 21 ```













