# Trou_de_souris


## Consignes :


Trouvez le flag sur le serveur


## Points attribués :
```
30 Points
```

## Flag :  

```
Fl@g{rz0>@ll}
```

## Déroulé :

Le serveur est une vm s'appelant "trou_de_souris" sur le portail VM.

On commence par scanner le serveur pour y découvrir les ports en écoute ainsi que les services.

Le service SSH est un leurre, il n'y a pas login possible.

Le service FTP est configuré en anonymous. En s'y connectant on trouve un fichier fw.sh

Aprés analyse du fichier, on voit que ce sont des règles de pare-feu dans lesquelles on peut voir cette règle:

```bash
iptables -A INPUT -p tcp --sport 42750 --dport 80 -j ACCEPT
```

Cette règle permet d'accéder à une page web uniquement si le port source de la requête est le 42750.

Pour se faire, il y a au moins 3 méthodes:

- Via scapy en créant un script permettant de forger une suite de requêtes (jusqu'au GET HTTP) avec le port source 42750 (script dans asset)

- Sous un système LINUX, en modifiant la valeur **/proc/sys/net/ipv4/ip_local_port_range** pour lui assigner uniquement le port en 42750

- En faisant une règle iptables permettant de le nat du port source:

```bash
iptables -t nat -A POSTROUTING -p tcp --sport 32768:60999 -j SNAT --to adresse_ip_source:42750
```

Ceci nous permet accéder à une page web dont le header contient le flag.

