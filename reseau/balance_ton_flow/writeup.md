

# Balance ton Flow

## Consignes :


Il vous est demandé d'analyser un pcap ayant interpellé le superviseur du SIEM de l'entreprise.


## Points attribués : ###

```
10 Points
```

## Flag : ### 

```
fl@g{1pv6_3xf1ltr@T10n}
```


## Déroulé : ###

Récupérer l'ipV6 source avec wireshark puis utilisation de tshark afin de récupérer les flows labels :

```bash
tshark -r capture.pcap -Y "ipv6.src==fe80::a00:27ff:fe39:4613" -T fields -e ipv6.flow > capture.raw
```

On nettoie le fichier :

```bash
sed -e 's/^0x000//g' -e '1d' capture.raw | xxd -r -p > capture.xxd
```

On peut voir que ce fichier est un gzip :

``` 
file capture.xxd
capture.xxd: gzip compressed data, last modified: Fri Dec 27 22:30:55 2019, max compression
mv capture.xxd capture.gz
gunzip capture.gz
```

On visualise le fichier :

```
cat capture

##############################################################
                     .ed"""" """$$$$be.
                   -"           ^""**$$$e.
                 ."                   '$$$c
                /                      "4$$b
               d  3                      $$$$
               $  *                   .$$$$$$
              .$  ^c           $$$$$e$$$$$$$$.
              d$L  4.         4$$$$$$$$$$$$$$b
              $$$$b ^ceeeee.  4$$ECL.F*$$$$$$$
  e$""=.      $$$$P d$$$$F $ $$$$$$$$$- $$$$$$
 z$$b. ^c     3$$$F "$$$$b   $"$$$$$$$  $$$$*"      .=""$c
4$$$$L        $$P"  "$$b   .$ $$$$$...e$$        .=  e$$$.
^*$$$$$c  %..   *c    ..    $$ 3$$$$$$$$$$eF     zP  d$$$$$
  "**$$$ec   "   %ce""    $$$  $$$$$$$$$$*    .r" =$$$$P""
        "*$b.  "c  *$e.    *** d$$$$$"L$$    .d"  e$$***"
          ^*$$c ^$c $$$      4J$$$$$% $$$ .e*".eeP"
             "$$$$$$"'$=e....$*$$**$cz$$" "..d$*"
               "*$$$  *=%4.$ L L$ P3$$$F $$$P"
                  "$   "%*ebJLzb$e$$$$$b $P"
                    %..      4$$$$$$$$$$ "
                     $$$e   z$$$$$$$$$$%
                      "*$c  "$$$$$$$P"
                       ."""*$$$$$$$$bc
                    .-"    .$***$$$"""*e.
                 .-"    .e$"     "*$c  ^*b.
          .=*""""    .e$*"          "*bc  "*$e..
        .$"        .z*"               ^*$e.   "*****e.
        $$ee$c   .d"                     "*$.        3.
        ^*$E")$..$"                         *   .ee==d%
           $.d$$$*                           *  J$$$e*
            """""                              "$$$"

###################fl@g{1pv6_3xf1ltr@T10n}####################
```

