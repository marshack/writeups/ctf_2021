#!/usr/bin/env  python3
import requests
import json
from bs4 import BeautifulSoup

s = requests.Session()

# Désactivation de l'utilisation d'un proxy
s.trust_env = False

# Récupération de la page d'index où se trouve l'ensemble des liens
r = s.get('http://192.168.120.1:8081/')

soup = BeautifulSoup(r.text, 'html.parser')

# Récupération de l'ensemble des liens
links = [a['href'] for a in soup.find_all('a') if '#' not in a['href']]

data_in_links = []

# Parcours des liens
for link in links:

    # Récupération de la page de chaque lien
    r = s.get('http://192.168.120.1:8081{}'.format(link))

    # Récupération du JSON présent dans la page
    dict_response = json.loads(r.text)

    # S'il existe des données
    if dict_response['data'] != None:

        data_in_links.append((dict_response['data'], dict_response['timestamp']))

# Trie des données pour les remettre dans le bon ordre
data_in_links.sort(key=lambda x:x[1])

# Assemblage des données binaires
binary_data_join = ''.join([_[0] for _ in data_in_links])

# Découpage de la chaine de données binaires pour la convertir en caractères ascii
binary_data_cut_to_ascii = ''.join([chr(int(binary_data_join[i:i+8], 2)) for i in range(0, len(binary_data_join) + 1, 8) if binary_data_join[i:i+8] != ''])

print(binary_data_cut_to_ascii)