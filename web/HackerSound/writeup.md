# HackerSounds

## Consignes :  


Une plateforme de partage de musique pour hacker vient d'être mise en ligne ! Essayez par tous les moyens de vous connecter à l'interface d'administration! 


## Points attribués :  

```
60 Points
```

## Flag :  
```
FL@G{FFmegIsFun}
```

## Déroulé :  


La page d'accueil est un formulaire d'upload de fichier. Il est écrit que le serveur n'acceptera que les fichiers au format avi.
Il existe une vulnérabilité sur FFmpeg qui permet de lire des fichiers du serveur via un upload de fichier avi.
Le binaire est disponible ici : https://raw.githubusercontent.com/swisskyrepo/PayloadsAllTheThings/master/Upload%20Insecure%20Files/CVE%20Ffmpeg%20HLS/gen_xbin_avi.py

On génére le payload comme ceci :

```
python3 gen_xbin_avi.py file:///var/www/login.php test.avi
```

Le fichier est uploadé et nous récupérons donc la sortie : un autre fichier avi dans lequel nous pouvons lire le user et le mot de passe de login.php.

Il ne nous reste plus qu'à utiliser les credentials pour récupérer le flag!
