
# Searching

## Consignes :  
<img src="binaire/apple.jpg" />


Vous recevez un mail contenant les éléments ci-dessous :
 - le texte suivant : 
```
Salut, je viens vers toi pour un coup de main. Je suis intrigué après avoir recu ce mail ce matin. As-tu une idée ? Merci par avance. (je te fais suivre le contenu)
"Jetons les masques sur notre situation. Il était le premier. Une pensée à lui. EYJBRTYOKMQIJ"
```
 - les pièces jointes suivantes : une image et un fichier.

Cela vous intrigue, il doit y avoir quelques chose à faire avec cela.


## Points attribués :  
```
20 Points
```

## Flag :  
```
Fl@g{Wilmslow} ou  Fl@g{wilmslow}
```

## Déroulé :  

L'analyse de l'image apple.png nous donne des informations :

```
hexdump apple.jpg

0001260 00 00 00 00 00 00 00 00 00 00 00 1f ff d9 68 74
0001270 74 70 73 3a 2f 2f 63 69 74 61 74 69 6f 6e 2d 63
0001280 65 6c 65 62 72 65 2e 6c 65 70 61 72 69 73 69 65
0001290 6e 2e 66 72 2f 61 75 74 65 75 72 2f 61 6c 61 6e
00012a0 2d 74 75 72 69 6e 67
00012a7
```

Il faut ensuite transformer les valeurs hexa en ascii : 
https://www.rapidtables.com/convert/number/hex-to-ascii.html

et nous obtenons :

```
https://citation-celebre.leparisien.fr/auteur/alan-turing
```

La citation est la clé pour déchiffrer le code via l'algorithme de verman.

```
texte : TURINGFOREVER

Clé : Les tentatives de création de machines pensantes nous seront d'une grande aide pour découvrir comment nous pensons nous-mêmes.

Chiffré : EYJBRTYOKMQIJ
```

Une fois cette information, nous pouvons déchiffrer le fichier data avec le mot de passe trouvé à l'étape précédente.

```
openssl enc -d -aes-256-cbc -in data -out fichier.png -k TURINGFOREVER
```

il faut cependant faire attention à la version d'openssl.
Si vous avez la version 1.1, installez la 1.0.2l

```
sudo apt-get install make 
wget https://www.openssl.org/source/openssl-1.0.2l.tar.gz 
tar -xzvf openssl-1.0.2l.tar.gz 
cd openssl-1.0.2l 
sudo ./config
sudo make install 
sudo ln -sf /usr/local/ssl/bin/openssl `which openssl` 

```

Il s'agit d'un Qrcode qui une fois scanné indique les positions suivantes :

<img src="assets/Qrcode.png" />

```
53° 19′ 30″ nord, 2° 14′ 20″ ouest
```

En allant sur GoogleMaps, elles indiquent la ville de Wilmslow.

Le flag est donc : Fl@g{Wilmslow} ou  Fl@g{wilmslow}